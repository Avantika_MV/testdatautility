import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class XML_Reader {

    public NodeList readXML(File xml, String Tag) {
        NodeList nList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xml);
            doc.getDocumentElement().normalize();
            nList = doc.getElementsByTagName(Tag);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nList;
    }

}