


import java.sql.ResultSet;

import java.time.ZonedDateTime;
import java.util.*;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;

public class TestMachine {

    JLabel lblNewLabel = new JLabel("");
    JTextArea textArea = new JTextArea();
    JTextArea textArea_1 = new JTextArea();
    JTextArea textArea_2 = new JTextArea();
    JTextArea textArea_3 = new JTextArea();
    JTextArea textArea_4 = new JTextArea();
    JTextArea textArea_5 = new JTextArea();
    private JFrame frmServiceTestingTool;
    private JTable table;
    private JTextField txtEnv;
    private JTextField textField;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JTextField textField6;
    private JTextField textField7;
    private JTextField textField8;
    private JTextField textField9;
    private JTextField textField10;
    ResultSet rs = null;
    ResultSet rsGlobal = null;
    FileWriter fw = null;
    String db_id = null;

    /**
     * Create the application.
     */
    public TestMachine() {
        initialize();
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) throws IOException {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    TestMachine window = new TestMachine();
                    window.frmServiceTestingTool.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //Expected Result function
    public static String statusReport(String actualOutput, String expectedOutput) {
        //System.out.println("inside Status report");
        String actualOutput1 = null;
        String result_verify = "PASS";
        String result_exists = "PASS";
        String result = "PASS";
        if (!(actualOutput == null)) {
            actualOutput = actualOutput.toLowerCase();
            actualOutput1 = actualOutput;
        }
        expectedOutput = expectedOutput.toLowerCase();
        String verify = expectedOutput.split(";")[0];
        String exists = expectedOutput.split(";")[1];
        String verify_content = verify.split("@")[1];
        //System.out.println("verify_content="+verify_content);
        String exists_content = exists.split("@")[1];
        //System.out.println("exists_content="+exists_content);
        int flag;
        try {
            if (!(exists_content.equalsIgnoreCase("null"))) {
                String arr_exists[] = exists_content.split(",");

                //code for exists

                for (int i = 0; i < arr_exists.length && result_verify.equalsIgnoreCase("PASS"); i++) {
                    flag = 0;
                    String temp = "";
                    //String val = "\"" + arr_exists[i] + "\"";
                    String val = arr_exists[i];
                    //System.out.println("val for split="+val);
                    if (actualOutput1 != null && actualOutput1.contains(val)) {

                        String val3 = "";
                        val3 = actualOutput1;
                        if (actualOutput1.contains("\"")) {
                            val3 = actualOutput1.split("\"" + val + "\"")[1].split(",")[0];
                        }
                        //val3 = val3.replaceAll("\"","");
                        //val3 = val3.replaceAll(":","");
                        else if (actualOutput1.contains("&")) {
                            val3 = actualOutput1.split(val)[1].split("&")[0];
                            //val3 = val3.replaceAll("=","");
                        }

                        else if (actualOutput1.contains(",")) {
                            val3 = actualOutput1.split(val)[1].split(",")[0];
                            //val3 = val3.replaceAll("=","");
                        }
                        //System.out.println("val3="+val3);

                        for (int j = 0; j < val3.length(); j++) {
                            char c = val3.charAt(j);
                            if ((!Character.isLetterOrDigit(c)) && flag == 0 && c != '_' && c != ' ' && c != '.' && c != '-' && c!='\'') {
                                continue;
                            } else if ((!Character.isLetterOrDigit(c)) && flag == 1 && c != '_' && c != ' ' && c != '.' && c != '-' && c!='\'') {
                                break;
                            } else {
                                flag = 1;
                                temp += c;
                            }

                        }
                        if (temp.length() < 1) {
                            result_exists = "FAIL";
                            break;
                        } else {
                            result_exists = "PASS";
                        }
                    } else {
                        result_exists = "FAIL";
                    }

                }

            } else {
                result_exists = "PASS";
            }

            if (!(verify_content.equals("null"))) {
                String arr_verify[] = verify_content.split(",");
                //for loop for verify
                for (int i = 0; i < arr_verify.length && result_verify.equalsIgnoreCase("PASS"); i++) {
                    //flag = 0;
                    //String temp = "";
                    String val = arr_verify[i];
                    //String val1 = "\"" + val.split(":")[0] + "\"";
                    String val1 = val.split(":")[0];   //success
                    String val2 = val.split(":")[1];   //true

                    if (actualOutput != null && actualOutput.contains("\"")) {
                        if (actualOutput.contains("\"" + val1 + "\"")) {
                            //System.out.println("inside if");
                            String val3 = "";
                            for (int k = 1; k < actualOutput.split("\"" + val1 + "\"").length; k++) {
                                val3 = actualOutput.split("\"" + val1 + "\"")[k];
                                String temp = "";
                                flag = 0;
                                for (int j = 0; j < val3.length(); j++) {
                                    char c = val3.charAt(j);
                                    if ((!Character.isLetterOrDigit(c)) && flag == 0 && c != '_' && c != ' ' && c != '.' && c != '-' && c!='\'') {
                                        continue;
                                    } else if ((!Character.isLetterOrDigit(c)) && flag == 1 && c != '_' && c != ' ' && c != '.' && c != '-' && c!='\'') {
                                        break;
                                    } else {
                                        flag = 1;
                                        temp += c;
                                    }

                                }
                                //System.out.println("temp="+temp);
                                if (!val2.equalsIgnoreCase(temp)) {
                                    //System.out.println("inside result verify if fail");
                                    result_verify = "FAIL";
                                    //System.out.println("check="+result_verify);
                                    continue;
                                } else {
                                    result_verify = "PASS";
                                    break;
                                }
                            }

                            //String val3 = actualOutput.split("\""+val1+"\"")[1];
                            //actualOutput = val3;
                            //System.out.println("val3="+val3);
                        } else {
                            result_verify = "FAIL";
                            break;
                        }
                    } else if ((actualOutput != null && actualOutput.contains("&")) || (actualOutput != null && actualOutput.contains(","))) {
                        if (actualOutput.contains(val1 + "=")) {
                            //System.out.println("inside if");
                            String val3 = "";
                            for (int k = 1; k < actualOutput.split(val1 + "=").length; k++) {
                                val3 = actualOutput.split(val1 + "=")[k];
                                String temp = "";
                                flag = 0;
                                for (int j = 0; j < val3.length(); j++) {
                                    char c = val3.charAt(j);
                                    if ((!Character.isLetterOrDigit(c)) && flag == 0 && c != '_' && c != ' ' && c != '.' && c != '-' && c!='\'') {
                                        continue;
                                    } else if ((!Character.isLetterOrDigit(c)) && flag == 1 && c != '_' && c != ' ' && c != '.' && c != '-' && c!='\'') {
                                        break;
                                    } else {
                                        flag = 1;
                                        temp += c;
                                    }

                                }
                                //System.out.println("temp="+temp);
                                if (!val2.equalsIgnoreCase(temp)) {
                                    //System.out.println("inside result verify if fail");
                                    result_verify = "FAIL";
                                    //System.out.println("check="+result_verify);
                                    continue;
                                } else {
                                    result_verify = "PASS";
                                    break;
                                }
                            }
                            //String val3 = actualOutput.split("\""+val1+"\"")[1];
                            //actualOutput = val3;
                            //System.out.println("val3="+val3);
                        } else {
                            result_verify = "FAIL";
                            break;
                        }
                    } else {
                        //System.out.println("inside does not contain in actual");
                        result_verify = "FAIL";
                    }


                }
                //
            } else {
                result_verify = "PASS";
            }


            //System.out.println("result_verify="+result_verify);
            //System.out.println("result_exists="+result_exists);
            boolean a_bool = !verify_content.equals("null");
            boolean b_bool = result_verify.equalsIgnoreCase("Pass");
            boolean c_bool = !exists_content.equals("null");
            boolean d_bool = result_exists.equalsIgnoreCase("Pass");

            if (!a_bool && !b_bool && !c_bool && !d_bool) {
                result = "N/A";
            } else if (!a_bool && !b_bool && !c_bool && d_bool) {
                result = "N/A";
            } else if (!a_bool && !b_bool && c_bool && !d_bool) {
                result = "FAIL";
            } else if (!a_bool && !b_bool && c_bool && d_bool) {
                result = "PASS";
            } else if (!a_bool && b_bool && !c_bool && !d_bool) {
                result = "N/A";
            } else if (!a_bool && b_bool && !c_bool && d_bool) {
                result = "PASS";
            } else if (!a_bool && b_bool && c_bool && !d_bool) {
                result = "FAIL";
            } else if (!a_bool && b_bool && c_bool && d_bool) {
                result = "PASS";
            } else if (a_bool && !b_bool && !c_bool && !d_bool) {
                result = "FAIL";
            } else if (a_bool && !b_bool && !c_bool && d_bool) {
                result = "FAIL";
            } else if (a_bool && !b_bool && c_bool && !d_bool) {
                result = "FAIL";
            } else if (a_bool && !b_bool && c_bool && d_bool) {
                result = "FAIL";
            } else if (a_bool && b_bool && !c_bool && !d_bool) {
                result = "PASS";
            } else if (a_bool && b_bool && !c_bool && d_bool) {
                result = "PASS";
            } else if (a_bool && b_bool && c_bool && !d_bool) {
                result = "FAIL";
            } else if (a_bool && b_bool && c_bool && d_bool) {
                result = "PASS";
            } else {
                result = "P/F";
            }
        } catch (Exception e) {
            result = "ERROR";
            e.printStackTrace();
        }
        //System.out.println("final result="+result);
        return result;

    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmServiceTestingTool = new JFrame();
        frmServiceTestingTool.setTitle("Repeat User DB Data Copy Utility");
        frmServiceTestingTool.getContentPane().setFont(new Font("Arial", Font.PLAIN, 10));
        frmServiceTestingTool.setBounds(10, 10, 1350, 1000);
        frmServiceTestingTool.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmServiceTestingTool.getContentPane().setLayout(null);

        JLabel lblServiceRequest = new JLabel("LENDING DB HOST");
        lblServiceRequest.setBounds(10, 700, 120, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest);

        textField = new JTextField();
        textField.setBounds(130, 700, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField);
        textField.setColumns(10);

        JLabel lblServiceRequest1 = new JLabel("DB USERNAME");
        lblServiceRequest1.setBounds(10, 730, 100, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest1);

        textField1 = new JTextField();
        textField1.setBounds(130, 730, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField1);
        textField1.setColumns(10);

        JLabel lblServiceRequest2 = new JLabel("DB PASSWORD");
        lblServiceRequest2.setBounds(10, 760, 100, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest2);

        textField2 = new JTextField();
        textField2.setBounds(130, 760, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField2);
        textField2.setColumns(10);


        JLabel lblServiceRequest3 = new JLabel("DATA DB HOST");
        lblServiceRequest3.setBounds(430, 700, 120, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest3);

        textField3 = new JTextField();
        textField3.setBounds(550, 700, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField3);
        textField3.setColumns(10);

        JLabel lblServiceRequest4 = new JLabel("DB USERNAME");
        lblServiceRequest4.setBounds(430, 730, 100, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest4);

        textField4 = new JTextField();
        textField4.setBounds(550, 730, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField4);
        textField4.setColumns(10);

        JLabel lblServiceRequest5 = new JLabel("DB PASSWORD");
        lblServiceRequest5.setBounds(430, 760, 100, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest5);

        textField5 = new JTextField();
        textField5.setBounds(550, 760, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField5);
        textField5.setColumns(10);

        JLabel lblServiceRequest6 = new JLabel("PORTAL ENV");
        lblServiceRequest6.setBounds(850, 700, 120, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest6);

        textField6 = new JTextField();
        textField6.setBounds(970, 700, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField6);
        textField6.setColumns(10);

        JLabel lblServiceRequest7 = new JLabel("USERNAME");
        lblServiceRequest7.setBounds(850, 730, 100, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest7);

        textField7 = new JTextField();
        textField7.setBounds(970, 730, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField7);
        textField7.setColumns(10);

        JLabel lblServiceRequest8 = new JLabel("PASSWORD");
        lblServiceRequest8.setBounds(850, 760, 100, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest8);

        textField8 = new JTextField();
        textField8.setBounds(970, 760, 275, 20);
        frmServiceTestingTool.getContentPane().add(textField8);
        textField8.setColumns(10);

        final JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 100, 1028, 287);
        frmServiceTestingTool.getContentPane().add(scrollPane);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(10, 471, 283, 72);
        frmServiceTestingTool.getContentPane().add(scrollPane_1);
        //JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setBounds(10, 436, 223, 14);
        frmServiceTestingTool.getContentPane().add(lblNewLabel);

        txtEnv = new JTextField();
        txtEnv.setText("jdbc:mysql://");
        txtEnv.setBounds(140, 11, 148, 20);
        //frmServiceTestingTool.getContentPane().add(txtEnv);
        //txtEnv.setColumns(10);
        //Set
        final JComboBox comboBox = new JComboBox();
        comboBox.setModel(new DefaultComboBoxModel(new String[]{"jdbc:mysql://"}));
        comboBox.setBounds(145, 11, 148, 20);
        frmServiceTestingTool.getContentPane().add(comboBox);
        txtEnv.setColumns(10);

        JLabel lblEnvironment = new JLabel("Environment/Domain");
        lblEnvironment.setBounds(10, 14, 157, 14);
        frmServiceTestingTool.getContentPane().add(lblEnvironment);

        JScrollPane scrollPane_8 = new JScrollPane();
        scrollPane_8.setBounds(763, 566, 275, 100);
        frmServiceTestingTool.getContentPane().add(scrollPane_8);
        //JTextArea textArea_4 = new JTextArea();
        scrollPane_8.setViewportView(textArea_4);
        textArea_4.setLineWrap(true);

        JScrollPane scrollPane_4 = new JScrollPane();
        scrollPane_4.setBounds(761, 565, 277, 102);
        frmServiceTestingTool.getContentPane().add(scrollPane_4);
        JLabel lblResponseBody = new JLabel("Response Body");
        lblResponseBody.setBounds(10, 550, 157, 14);
        frmServiceTestingTool.getContentPane().add(lblResponseBody);

        JLabel lblResponseHeader = new JLabel("Response Header");
        lblResponseHeader.setBounds(763, 550, 190, 14);
        frmServiceTestingTool.getContentPane().add(lblResponseHeader);

        JLabel lblRequestHeader = new JLabel("Request Header");
        lblRequestHeader.setBounds(10, 454, 135, 14);
        frmServiceTestingTool.getContentPane().add(lblRequestHeader);

        JLabel lblRequest = new JLabel("Request");
        lblRequest.setBounds(303, 453, 91, 14);
        frmServiceTestingTool.getContentPane().add(lblRequest);

        JScrollPane scrollPane_2 = new JScrollPane();
        scrollPane_2.setBounds(303, 471, 265, 72);
        frmServiceTestingTool.getContentPane().add(scrollPane_2);

        //JTextArea textArea_1 = new JTextArea();
        textArea_1.setLineWrap(true);
        scrollPane_2.setViewportView(textArea_1);

        JLabel lblRequestParameter = new JLabel("Request Parameter");
        lblRequestParameter.setBounds(578, 454, 148, 14);
        frmServiceTestingTool.getContentPane().add(lblRequestParameter);

        JScrollPane scrollPane_7 = new JScrollPane();
        scrollPane_7.setBounds(10, 566, 743, 100);
        frmServiceTestingTool.getContentPane().add(scrollPane_7);

        //JTextArea textArea_3 = new JTextArea();
        scrollPane_7.setViewportView(textArea_3);
        textArea_3.setLineWrap(true);

        JScrollPane scrollPane_5 = new JScrollPane();
        scrollPane_5.setBounds(10, 564, 743, 102);
        frmServiceTestingTool.getContentPane().add(scrollPane_5);

        JScrollPane scrollPane_6 = new JScrollPane();
        scrollPane_6.setBounds(578, 472, 460, 70);
        frmServiceTestingTool.getContentPane().add(scrollPane_6);

        final JLabel lblNewLabel_1 = new JLabel("");
        lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
        lblNewLabel_1.setBounds(763, 411, 275, 14);
        frmServiceTestingTool.getContentPane().add(lblNewLabel_1);


        //JTextArea textArea_2 = new JTextArea();
        scrollPane_6.setViewportView(textArea_2);
        textArea_2.setLineWrap(true);

        JScrollPane scrollPane_3 = new JScrollPane();
        scrollPane_3.setBounds(576, 471, 462, 72);
        frmServiceTestingTool.getContentPane().add(scrollPane_3);
        final JTextArea textArea = new JTextArea();
        textArea.setLineWrap(true);
        scrollPane_1.setViewportView(textArea);


        JLabel lblServiceRequest9 = new JLabel("PROD DB USERNAME");
        lblServiceRequest9.setBounds(1050, 600, 130, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest9);

        textField9 = new JTextField();
        textField9.setBounds(1180, 600, 160, 20);
        frmServiceTestingTool.getContentPane().add(textField9);
        textField9.setColumns(10);

        JLabel lblServiceRequest10 = new JLabel("PROD DB PASSWORD");
        lblServiceRequest10.setBounds(1050, 630, 130, 14);
        frmServiceTestingTool.getContentPane().add(lblServiceRequest10);

        textField10 = new JTextField();
        textField10.setBounds(1180, 630, 160, 20);
        frmServiceTestingTool.getContentPane().add(textField10);
        textField10.setColumns(10);



        table = new JTable();
        scrollPane.setViewportView(table);

        //TEST-SUITE
        //table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        JLabel lblSuiteHeading = new JLabel("Test Suite Panel");
        lblSuiteHeading.setFont(new Font("Arial", Font.BOLD, 14));
        lblSuiteHeading.setBounds(1137, 50, 125, 14);
        //lblSuiteHeading.setHorizontalAlignment();
        //lblSuiteHeading.setBounds(1500, 50, 125, 14);
        frmServiceTestingTool.getContentPane().add(lblSuiteHeading);

        JScrollPane scrollPane_ts = new JScrollPane();
        scrollPane_ts.setBounds(1050, 155, 275, 230);
        frmServiceTestingTool.getContentPane().add(scrollPane_ts);

        //JTextArea textArea_5 = new JTextArea();
        textArea_5.setLineWrap(true);
        scrollPane_ts.setViewportView(textArea_5);

        JLabel lblSelect = new JLabel("Test Suite");
        lblSelect.setBounds(1053, 92, 100, 20);
        //lblSelect.setBounds(1400, 92, 100, 20);
        frmServiceTestingTool.getContentPane().add(lblSelect);

        System.out.println(System.getProperty("user.dir") + "/Workflows");

        File directory = new File(System.getProperty("user.dir") + "/Workflows");

        // get all the files from a directory
        File[] fList = directory.listFiles();
        String[] Workflow_files = new String[fList.length];
        int count_files = 0;
        for (File file : fList) {

            Workflow_files[count_files] = file.getName().toString().split(".xml")[0];
            //model.addElement(file.getName().toString());
            count_files++;
        }


        final JComboBox comboBox_1 = new JComboBox();
        //comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Enroll","Login"}));
        comboBox_1.setModel(new DefaultComboBoxModel(Workflow_files));
        comboBox_1.setBounds(1153, 92, 167, 20);
        //comboBox_1.setBounds(1500, 92, 167, 20);
        frmServiceTestingTool.getContentPane().add(comboBox_1);


        //////// Select

        JButton btnSelectSuite = new JButton("Select");
        btnSelectSuite.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //DefaultTableModel model =  (DefaultTableModel)table.getModel();
                // int[] rows = table.getSelectedRows();
                try {

                    //
                    String finalText = "";
                    String existingText = textArea_5.getText();
                    //System.out.println("Existing Text = "+existingText);
                    String selectedText = comboBox_1.getSelectedItem().toString();
                    //System.out.println("Selected Text = "+selectedText);
                    if (existingText != null)
                        finalText = existingText + selectedText + "\n";
                    else
                        finalText = selectedText + "\n";
                    textArea_5.setText(finalText);
                } catch (Exception E) {
                    JOptionPane.showMessageDialog(null, "Please select a Test Suite");
                }
            }
        });
        btnSelectSuite.setFont(new Font("Arial", Font.PLAIN, 12));
        btnSelectSuite.setBounds(1218, 120, 102, 23);
        //btnSelectSuite.setBounds(1550, 120, 102, 23);
        frmServiceTestingTool.getContentPane().add(btnSelectSuite);

        //////// Clear

        JButton btnClearSuite = new JButton("Clear");
        btnClearSuite.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //DefaultTableModel model =  (DefaultTableModel)table.getModel();
                // int[] rows = table.getSelectedRows();
                try {

                    //

                    String finalText = null;
                    textArea_5.setText(finalText);
                } catch (Exception E) {
                    JOptionPane.showMessageDialog(null, "There is no Test Suite");
                }
            }
        });
        btnClearSuite.setFont(new Font("Arial", Font.PLAIN, 12));
        btnClearSuite.setBounds(1053, 120, 102, 23);
        //btnClearSuite.setBounds(1400, 120, 102, 23);
        frmServiceTestingTool.getContentPane().add(btnClearSuite);

        //Separator - vertical
        JSeparator separator_v = new JSeparator();
        separator_v.setBounds(1042, 10, 2, 690);
        frmServiceTestingTool.getContentPane().add(separator_v);


        //Run All Test Suites
        JButton btnRunAllTestSuites = new JButton("Run Test Suite");
        btnRunAllTestSuites.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //System.out.println("inside run all test suites");
                URL url = null;
                lblNewLabel_1.setText("Please Wait.....");
                lblNewLabel_1.paintImmediately(lblNewLabel_1.getVisibleRect());
                String runText = textArea_5.getText();
                //System.out.println("Run Text ="+runText);
                String arr_testSuites[] = runText.split("\n");
                //System.out.println("array length = "+arr_testSuites.length+"\nsplited run text:");

                //for (int i = 0; i < arr_testSuites.length; i++)
                //System.out.println(arr_testSuites[i]);

                //New Code
                for (int outerb = 0; outerb < arr_testSuites.length; outerb++) {


                    String test_name = arr_testSuites[outerb].trim();
                    //System.out.println("first test name="+arr_testSuites[outerb].trim());

                    //XML Code
                    try {
                        File path = new File(System.getProperty("user.dir") + "/Workflows/" + test_name + ".xml");
                        XML_Reader xmlInput_1 = new XML_Reader();
                        NodeList nList_1 = xmlInput_1.readXML(path, "testcase");

                        Element element_1 = (Element) nList_1.item(0);

                        NodeList nList_2 = element_1.getElementsByTagName("TestName");

                        String TestName[] = new String[nList_2.getLength()];

                        int i = 0;
                        for (int temp = 0; temp < nList_2.getLength(); temp++, i++) {
                            Node nNode = nList_2.item(temp);

                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                                Element line = (Element) nList_2.item(temp);
                                TestName[i] = line.getTextContent();

                                for (int j = 0; j < table.getRowCount(); j++) {

                                    String col_val = table.getModel().getValueAt(j, 1).toString();
                                    if (col_val.equals(TestName[i])) {
                                        table.getModel().setValueAt(true, j, 0);
                                    }
                                }

                            } else {
                                System.out.println("No Test Case Found");
                            }
                        }
                    } catch (Exception e_testSuite) {
                        System.out.println("Test Suite xml file not found.");
                    }


                    //calling Run Button
                    //
                    for (int b = 0; b < table.getRowCount(); b++) {

                        Boolean Execute = (Boolean) table.getModel().getValueAt(b, 0);
                        String Test_name = (String) table.getModel().getValueAt(b, 1);
                        String Request_Type = (String) table.getModel().getValueAt(b, 2);
                        String Content_Type = (String) table.getModel().getValueAt(b, 3);
                        String Service_Request1 = (String) table.getModel().getValueAt(b, 4);
                        String hostUrl = textField.getText();
                        String username = textField1.getText();
                        String password = textField2.getText();
                        String data_hostUrl = textField3.getText();
                        String data_username = textField4.getText();
                        String data_password = textField5.getText();
                        String portal_hostUrl = textField6.getText();
                        String portal_username = textField7.getText();
                        String portal_password = textField8.getText();
                        String prod_username = textField9.getText();
                        String prod_password = textField10.getText();
                        String Request_Header = "";
                        String Service_Request = comboBox.getSelectedItem() + Service_Request1;
                        if (Execute) {
                            if (Service_Request1.startsWith("http") )   {
                                Service_Request = Service_Request1;
                            }  if (Service_Request1.startsWith("jdbc")) {
                                Service_Request = Service_Request1;
                            }  if (Service_Request1.equalsIgnoreCase("%hostUrl%")) {
                                Service_Request = comboBox.getSelectedItem() + hostUrl;
                            }  if (Service_Request1.equalsIgnoreCase("%data_hostUrl%")) {
                                Service_Request = comboBox.getSelectedItem() + data_hostUrl;
                            }  if (Service_Request1.contains("%portal_environment%")) {
                                Service_Request = Service_Request1.replace("%portal_environment%", portal_hostUrl);
                            }

                        table.getModel().setValueAt(Service_Request, b, 4);
                        String Request_Parameter = (String) table.getModel().getValueAt(b, 5);
                        String val_req = Request_Parameter;
                        String status_val = table.getModel().getValueAt(b, 9).toString();


                        //
                        table.getModel().setValueAt(val_req, b, 5);
                        Request_Parameter = (String) table.getModel().getValueAt(b, 5);
                        //String Request_Parameter=comboBox.getSelectedItem()+" : "+Request_Parameter1;
                        //System.out.println("Service_Request="+Service_Request);
                        Request_Header = (String) table.getModel().getValueAt(b, 6);
                        if (Request_Header.equalsIgnoreCase("%credentials%") && !(Request_Header.startsWith("user"))) {
                            if (username!=null && password!=null)
                                Request_Header = "username:"+username+",password:"+password;
                            else
                                Request_Header = null;
                        } else if (Request_Header.equalsIgnoreCase("%data_credentials%") && !(Request_Header.startsWith("user"))) {
                            if (username!=null && password!=null)
                                Request_Header = "username:"+data_username+",password:"+data_password;
                            else
                                Request_Header = null;
                        } else if (Request_Header.equalsIgnoreCase("%portal_credentials%") && !(Request_Header.startsWith("user"))) {
                            if (username!=null && password!=null)
                                Request_Header = "username:"+portal_username+",password:"+portal_password;
                            else
                                Request_Header = null;
                        } else if (Request_Header.equalsIgnoreCase("%prod_credentials%") && !(Request_Header.startsWith("user"))) {
                            if (username!=null && password!=null)
                                Request_Header = "username:"+prod_username+",password:"+prod_password;
                            else
                                Request_Header = null;
                        }
                        table.getModel().setValueAt(Request_Header, b, 6);

                         if (Execute && Request_Type.equalsIgnoreCase("Trecvalidate")) {
                             try {
                                 Trecvalidate(Test_name, Service_Request, Request_Header, b);

                                 //Test Name
                                 lblNewLabel.setText("Test Name" + " : " + table.getModel().getValueAt(table.getSelectedRow(), 1).toString());

                                 //Service Request
                                 textArea_1.setText(table.getModel().getValueAt(table.getSelectedRow(), 4).toString());

                                 //Request Parameter
                                 textArea_2.setText(table.getModel().getValueAt(table.getSelectedRow(), 5).toString());

                                 //Request Header
                                 textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 6).toString());

                                 //Service Response
                                 textArea_3.setText(table.getModel().getValueAt(table.getSelectedRow(), 7).toString());

                                 //Response Header
                                 textArea_4.setText(table.getModel().getValueAt(table.getSelectedRow(), 8).toString());


                             }
                             catch (Exception E)    {

                             }
                         }

                        if (Execute && Request_Type.equalsIgnoreCase("DB_Validation")) {
                            //Select code
                            try {
                                dbValidation(Test_name, Service_Request, Request_Parameter, Request_Header, Content_Type, b);

                                //Test Name
                                lblNewLabel.setText("Test Name" + " : " + table.getModel().getValueAt(table.getSelectedRow(), 1).toString());

                                //Service Request
                                textArea_1.setText(table.getModel().getValueAt(table.getSelectedRow(), 4).toString());

                                //Request Parameter
                                textArea_2.setText(table.getModel().getValueAt(table.getSelectedRow(), 5).toString());

                                //Request Header
                                textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 6).toString());

                                //Service Response
                                textArea_3.setText(table.getModel().getValueAt(table.getSelectedRow(), 7).toString());

                                //Response Header
                                textArea_4.setText(table.getModel().getValueAt(table.getSelectedRow(), 8).toString());

                            } catch (Exception E) {
                                //JOptionPane.showMessageDialog(null, "Please select a Test Case");
                            }
                        }



                    }
                    }
                }
                try {
                    Thread.sleep(5000);
                    lblNewLabel_1.setText("Done");
                    lblNewLabel_1.paintImmediately(lblNewLabel_1.getVisibleRect());
                } catch (InterruptedException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });

        btnRunAllTestSuites.setFont(new Font("Arial", Font.PLAIN, 12));
        btnRunAllTestSuites.setBounds(1090, 402, 200, 23);
        //btnRunAllTestSuites.setBounds(1750, 402, 200, 23);
        frmServiceTestingTool.getContentPane().add(btnRunAllTestSuites);
        //closing Run All Button


        //TEST-SUITE


        //Check all
        JButton btnCheckAll = new JButton("Check All");
        btnCheckAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    ////System.out.println("ko:"+table.getRowCount());
                    for (int b = 0; b < table.getRowCount(); b++) {
                        table.getModel().setValueAt(true, b, 0);
                    }
                } catch (Exception er) {
                }
            }
        });
        btnCheckAll.setFont(new Font("Arial", Font.PLAIN, 12));
        btnCheckAll.setBounds(403, 10, 110, 23);
        frmServiceTestingTool.getContentPane().add(btnCheckAll);
        //
        //UnCheck all
        JButton btnUnCheckAll = new JButton("UnCheck All");
        btnUnCheckAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    ////System.out.println("ko:"+table.getRowCount());
                    for (int b = 0; b < table.getRowCount(); b++) {
                        table.getModel().setValueAt(false, b, 0);
                    }
                } catch (Exception er) {
                }
            }
        });
        btnUnCheckAll.setFont(new Font("Arial", Font.PLAIN, 12));
        btnUnCheckAll.setBounds(534, 10, 110, 23);
        frmServiceTestingTool.getContentPane().add(btnUnCheckAll);
        //


        File path = new File(System.getProperty("user.dir") + "/TestCases/TestCases1.xml");
        XML_Reader xmlInput = new XML_Reader();
        NodeList nList = xmlInput.readXML(path, "testcase");
        String TestName = null, InputParam = null, InputParamVal = null, OutputParamVal = null, Link = null, OutputParam = null, ContentType = null, RequestType = null, ServiceRequest = null, RequestParameter = null, RequestHeader = null, ServiceResponse = null, ResponseHeader = null, Status = null, InputHeaderFromPreviousResponse = null, InputResponseFromPreviousResponse = null, Code = null, Time = null, ExpectedResult = null;
        DefaultTableModel Table_config = new DefaultTableModel();

        table = new JTable(Table_config);
        String[] header = {"Execute", "TestName", "RequestType", "ContentType", "ServiceRequest", "RequestParameter", "RequestHeader", "ServiceResponse", "ResponseHeader", "Status", "InputParam", "InputParamVal", "OutputParam", "OutputParamVal", "Link", "Code", "Time (ms)", "ExpectedResult"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, header);
        table = new JTable(model) {

            private static final long serialVersionUID = 1L;

            /*@Override
            public Class getColumnClass(int column) {
            return getValueAt(0, column).getClass();
            }*/
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Boolean.class;

                    default:
                        return String.class;
                }
            }
        };
        ////
        //table.setModel(new DefaultTableModel(new Object[][]{},header));

        table.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseClicked(java.awt.event.MouseEvent e) {

                int row = table.rowAtPoint(e.getPoint());

                int col = table.columnAtPoint(e.getPoint());

                if (col != 0) {
                    //JTextArea tA = new JTextArea("<html><body><p style='width: 200px;'>" + table.getValueAt(row, col).toString() + "</p></body></html>");
                    JTextArea tA = new JTextArea(table.getValueAt(row, col).toString());
                    tA.setEditable(true);
                    tA.setDragEnabled(true);
                    JOptionPane.showMessageDialog(
                            null,
                            tA,
                            table.getColumnName(col),
                            JOptionPane.INFORMATION_MESSAGE);

                    //MessageDialogWithCopy.openInformation(null, table.getColumnName(col), table.getValueAt(row, col).toString());

                }
            }
        });


        table.setShowGrid(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        int height = 30;
        table.setRowHeight(height);

        int numCols = table.getModel().getColumnCount();

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                //Boolean Execute=(boolean)eElement.getElementsByTagName("TestName").item(0).getTextContent();
                Boolean Execute = true;
                TestName = eElement.getElementsByTagName("TestName").item(0).getTextContent();
                ContentType = eElement.getElementsByTagName("RequestType").item(0).getTextContent();
                RequestType = eElement.getElementsByTagName("ContentType").item(0).getTextContent();
                ServiceRequest = eElement.getElementsByTagName("ServiceRequest").item(0).getTextContent();
                RequestParameter = eElement.getElementsByTagName("RequestParameter").item(0).getTextContent();
                RequestHeader = eElement.getElementsByTagName("RequestHeader").item(0).getTextContent();
                ServiceResponse = eElement.getElementsByTagName("ServiceResponse").item(0).getTextContent();
                ResponseHeader = eElement.getElementsByTagName("ResponseHeader").item(0).getTextContent();
                Status = eElement.getElementsByTagName("Status").item(0).getTextContent();
                InputParam = eElement.getElementsByTagName("InputParam").item(0).getTextContent();
                InputParamVal = eElement.getElementsByTagName("InputParamVal").item(0).getTextContent();
                OutputParam = eElement.getElementsByTagName("OutputParam").item(0).getTextContent();
                OutputParamVal = eElement.getElementsByTagName("OutputParamVal").item(0).getTextContent();
                Link = eElement.getElementsByTagName("Link").item(0).getTextContent();
                Code = eElement.getElementsByTagName("Code").item(0).getTextContent();
                Time = eElement.getElementsByTagName("Time").item(0).getTextContent();
                ExpectedResult = eElement.getElementsByTagName("ExpectedResult").item(0).getTextContent();


                Object[] file = new Object[numCols];
                file[0] = Boolean.FALSE;
                file[1] = TestName;
                file[2] = ContentType;
                file[3] = RequestType;
                file[4] = ServiceRequest;
                file[5] = RequestParameter;
                file[6] = RequestHeader;
                file[7] = ServiceResponse;
                file[8] = ResponseHeader;
                file[9] = Status;
                file[10] = InputParam;
                file[11] = InputParamVal;
                file[12] = OutputParam;
                file[13] = OutputParamVal;
                file[14] = Link;
                file[15] = Code;
                file[16] = Time;
                file[17] = ExpectedResult;

                ((DefaultTableModel) table.getModel()).addRow(file);

                scrollPane.setViewportView(table);


            }

        }
        table.getModel().setValueAt(false, 1, 0);

        JButton btnNewButton = new JButton("Select");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //DefaultTableModel model =  (DefaultTableModel)table.getModel();
                // int[] rows = table.getSelectedRows();
                try {

                    //Test Name
                    lblNewLabel.setText("Test Name" + " : " + table.getModel().getValueAt(table.getSelectedRow(), 1).toString());
                    //Service Request
                    textArea_1.setText(table.getModel().getValueAt(table.getSelectedRow(), 4).toString());
                    //Request Parameter
                    textArea_2.setText(table.getModel().getValueAt(table.getSelectedRow(), 5).toString());
                    //Request Header
                    textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 6).toString());
                    //Service Response
                    textArea_3.setText(table.getModel().getValueAt(table.getSelectedRow(), 7).toString());
                    //Response Header
                    textArea_4.setText(table.getModel().getValueAt(table.getSelectedRow(), 8).toString());

                } catch (Exception E) {
                    JOptionPane.showMessageDialog(null, "Please select a Test Case");
                }
            }
        });
        btnNewButton.setFont(new Font("Arial", Font.PLAIN, 12));
        btnNewButton.setBounds(10, 402, 82, 23);
        //frmServiceTestingTool.getContentPane().add(btnNewButton);

        JButton btnRun = new JButton("Run");
        btnRun.addActionListener(new ActionListener() {
                                     public void actionPerformed(ActionEvent e) {
                                         URL url = null;
                                         lblNewLabel_1.setText("Please Wait.....");
                                         lblNewLabel_1.paintImmediately(lblNewLabel_1.getVisibleRect());


                                         //New Code
                                         for (int b = 0; b < table.getRowCount(); b++) {

                                             Boolean Execute = (Boolean) table.getModel().getValueAt(b, 0);
                                             String Test_name = (String) table.getModel().getValueAt(b, 1);
                                             String Request_Type = (String) table.getModel().getValueAt(b, 2);
                                             String Content_Type = (String) table.getModel().getValueAt(b, 3);
                                             String Service_Request1 = (String) table.getModel().getValueAt(b, 4);
                                             String hostUrl = textField.getText();
                                             String username = textField1.getText();
                                             String password = textField2.getText();
                                             String data_hostUrl = textField3.getText();
                                             String data_username = textField4.getText();
                                             String data_password = textField5.getText();
                                             String portal_hostUrl = textField6.getText();
                                             String portal_username = textField7.getText();
                                             String portal_password = textField8.getText();
                                             String prod_username = textField9.getText();
                                             String prod_password = textField10.getText();
                                             String Request_Header = "";
                                             String Service_Request = comboBox.getSelectedItem() + Service_Request1;
                                             if (Execute) {
                                                 if (Service_Request1.startsWith("http") )   {
                                                     Service_Request = Service_Request1;
                                                 } if (Service_Request1.startsWith("jdbc")) {
                                                     Service_Request = Service_Request1;
                                                 } if (Service_Request1.equalsIgnoreCase("%hostUrl%")) {
                                                     Service_Request = comboBox.getSelectedItem() + hostUrl;
                                                 } if (Service_Request1.equalsIgnoreCase("%data_hostUrl%")) {
                                                     Service_Request = comboBox.getSelectedItem() + data_hostUrl;
                                                 } if (Service_Request1.contains("%portal_environment%")) {
                                                     Service_Request = Service_Request1.replace("%portal_environment%", portal_hostUrl);
                                                 }

                                                 table.getModel().setValueAt(Service_Request, b, 4);
                                                 String Request_Parameter = (String) table.getModel().getValueAt(b, 5);
                                                 String val_req = Request_Parameter;
                                                 String status_val = table.getModel().getValueAt(b, 9).toString();


                                                 //
                                                 table.getModel().setValueAt(val_req, b, 5);
                                                 Request_Parameter = (String) table.getModel().getValueAt(b, 5);
                                                 //String Request_Parameter=comboBox.getSelectedItem()+" : "+Request_Parameter1;
                                                 //System.out.println("Service_Request="+Service_Request);
                                                 Request_Header = (String) table.getModel().getValueAt(b, 6);
                                                 if (Request_Header.equalsIgnoreCase("%credentials%") && !(Request_Header.startsWith("user"))) {
                                                     if (username!=null && password!=null)
                                                         Request_Header = "username:"+username+",password:"+password;
                                                     else
                                                         Request_Header = null;
                                                 } else if (Request_Header.equalsIgnoreCase("%data_credentials%") && !(Request_Header.startsWith("user"))) {
                                                     if (username!=null && password!=null)
                                                         Request_Header = "username:"+data_username+",password:"+data_password;
                                                     else
                                                         Request_Header = null;
                                                 } else if (Request_Header.equalsIgnoreCase("%portal_credentials%") && !(Request_Header.startsWith("user"))) {
                                                     if (username!=null && password!=null)
                                                         Request_Header = "username:"+portal_username+",password:"+portal_password;
                                                     else
                                                         Request_Header = null;
                                                 } else if (Request_Header.equalsIgnoreCase("%prod_credentials%") && !(Request_Header.startsWith("user"))) {
                                                     if (username!=null && password!=null)
                                                         Request_Header = "username:"+prod_username+",password:"+prod_password;
                                                     else
                                                         Request_Header = null;
                                                 }
                                                 table.getModel().setValueAt(Request_Header, b, 6);

                                                 if (Execute && Request_Type.equalsIgnoreCase("Trecvalidate")) {
                                                     try {
                                                         Trecvalidate(Test_name, Service_Request, Request_Header, b);

                                                         //Test Name
                                                         lblNewLabel.setText("Test Name" + " : " + table.getModel().getValueAt(table.getSelectedRow(), 1).toString());

                                                         //Service Request
                                                         textArea_1.setText(table.getModel().getValueAt(table.getSelectedRow(), 4).toString());

                                                         //Request Parameter
                                                         textArea_2.setText(table.getModel().getValueAt(table.getSelectedRow(), 5).toString());

                                                         //Request Header
                                                         textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 6).toString());

                                                         //Service Response
                                                         textArea_3.setText(table.getModel().getValueAt(table.getSelectedRow(), 7).toString());

                                                         //Response Header
                                                         textArea_4.setText(table.getModel().getValueAt(table.getSelectedRow(), 8).toString());


                                                     }
                                                     catch (Exception E)    {

                                                     }
                                                 }

                                                 if (Execute && Request_Type.equalsIgnoreCase("DB_Validation")) {
                                                     //Select code
                                                     try {
                                                         dbValidation(Test_name, Service_Request, Request_Parameter, Request_Header, Content_Type, b);

                                                         //
                                                         lblNewLabel.setText("Test Name" + " : " + table.getModel().getValueAt(table.getSelectedRow(), 1).toString());
                                                         //Service Request
                                                         textArea_1.setText(table.getModel().getValueAt(table.getSelectedRow(), 4).toString());
                                                         //Request Parameter
                                                         textArea_2.setText(table.getModel().getValueAt(table.getSelectedRow(), 5).toString());
                                                         //Request Header
                                                         textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 6).toString());
                                                         //Service Response
                                                         textArea_3.setText(table.getModel().getValueAt(table.getSelectedRow(), 7).toString());
                                                         //REsponse Header
                                                         textArea_4.setText(table.getModel().getValueAt(table.getSelectedRow(), 8).toString());
                                                         //textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());
                                                     } catch (Exception E) {
                                                         E.printStackTrace();
                                                         //JOptionPane.showMessageDialog(null, "Please select a Test Case");
                                                     }
                                                 }
                                             }
                                         }
                                         lblNewLabel_1.setText("Done");
                                         lblNewLabel_1.paintImmediately(lblNewLabel_1.getVisibleRect());


                                         //

                                     }

                                 }

        );


        btnRun.setFont(new Font("Arial", Font.PLAIN, 12));
        btnRun.setBounds(10, 402, 82, 23);
        frmServiceTestingTool.getContentPane().add(btnRun);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 398, 1028, 2);
        frmServiceTestingTool.getContentPane().add(separator);

        JButton btnSaveTable = new JButton("Save Table");
        btnSaveTable.setFont(new Font("Arial", Font.PLAIN, 12));
        btnSaveTable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //System.out.println("inside save table");

                Writer xml_file = null;
                File statText = null;
                FileOutputStream is = null;
                OutputStreamWriter osw = null;
                String newline = System.getProperty("line.separator");
                try {
                    statText = new File(System.getProperty("user.dir") + "\\TestCases\\TestCases1.xml");
                    //System.out.println("opened the file");
                    is = new FileOutputStream(statText);
                    osw = new OutputStreamWriter(is, "UTF8");
                    xml_file = new BufferedWriter(osw);
                    DefaultTableModel dtm = (DefaultTableModel) table.getModel();
                    int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
                    xml_file.write("<?xml version=\"1.0\"?>" + newline + "<data>");
                    for (int i = 0; i < nRow; i++) {
                        //System.out.println("inside for loop of add test case");
                        xml_file.write(newline + "<testcase>");
                        for (int j = 1; j < nCol; j++) {
                            xml_file.write(newline);
                            String ColumnName = table.getModel().getColumnName(j);
                            xml_file.write("<" + ColumnName + ">");
                            //System.out.println(ColumnName);
                            String value = dtm.getValueAt(i, j).toString();
                            xml_file.write(value.replaceAll("&", "&amp;"));
                            //System.out.println("testname="+value);
                            //..replaceAll("&","&amp;"));
                            xml_file.write("</" + ColumnName + ">");
                            //xml_file.write(newline+"<"+table.getModel().getColumnName(j)+">"+dtm.getValueAt(i,j).toString().replaceAll("&", "&amp;")+"</"+table.getModel().getColumnName(j)+">");
                        }
                        xml_file.write(newline + "</testcase>");
                    }
                    xml_file.write(newline + "</data>");
                    xml_file.close();
                    JOptionPane.showMessageDialog(null, "Saved The Table");
                } catch (Exception Ex) {
                    JOptionPane.showMessageDialog(null, "Unable to Save Table");

                }


            }
        });
        btnSaveTable.setBounds(579, 66, 110, 23);
        frmServiceTestingTool.getContentPane().add(btnSaveTable);

        JButton btnMoveUp = new JButton("Move Up");
        btnMoveUp.setFont(new Font("Arial", Font.PLAIN, 12));
        btnMoveUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    DefaultTableModel model = (DefaultTableModel) table.getModel();

                    int[] rows = table.getSelectedRows();
                    model.moveRow(rows[0], rows[rows.length - 1], rows[0] - 1);
                    table.setRowSelectionInterval(rows[0] - 1, rows[rows.length - 1] - 1);
                } catch (Exception E2) {

                }

            }
        });
        btnMoveUp.setBounds(838, 66, 89, 23);
        frmServiceTestingTool.getContentPane().add(btnMoveUp);

        JButton btnMoveDown = new JButton("Move Down");
        btnMoveDown.setFont(new Font("Arial", Font.PLAIN, 12));
        btnMoveDown.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    DefaultTableModel model = (DefaultTableModel) table.getModel();
                    int[] rows = table.getSelectedRows();
                    model.moveRow(rows[0], rows[rows.length - 1], rows[0] + 1);
                    table.setRowSelectionInterval(rows[0] + 1, rows[rows.length - 1] + 1);
                } catch (Exception e3) {

                }

            }
        });
        btnMoveDown.setBounds(937, 66, 101, 23);
        frmServiceTestingTool.getContentPane().add(btnMoveDown);

        JButton btnDeleteSelected = new JButton("Delete Selected");
        btnDeleteSelected.setFont(new Font("Arial", Font.PLAIN, 12));
        btnDeleteSelected.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                DefaultTableModel model = (DefaultTableModel) table.getModel();

                //new code
                for (int i = 0; i < model.getRowCount(); i++) {
                    Boolean checked = (Boolean) model.getValueAt(i, 0);
                    if (checked != null && checked) {
                        model.removeRow(i);
                        i--;
                    }
                }
                //
				/*  int[] rows = table.getSelectedRows();
				   for(int i=0;i<rows.length;i++){
				     model.removeRow(rows[i]-i);
				   } */
                //JOptionPane.showMessageDialog(null, "Removed the Test Case, Please Save the Run Manager");

            }
        });
        btnDeleteSelected.setBounds(699, 66, 129, 23);
        frmServiceTestingTool.getContentPane().add(btnDeleteSelected);

        JButton btnReload = new JButton("Refresh Table");
        btnReload.setFont(new Font("Arial", Font.PLAIN, 12));
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                table = new JTable();
                scrollPane.setViewportView(table);


                //////////////////////////////////
                File path = new File(System.getProperty("user.dir") + "/TestCases/TestCases1.xml");
                XML_Reader xmlInput = new XML_Reader();
                NodeList nList = xmlInput.readXML(path, "testcase");
                String TestName = null, InputParam = null, InputParamVal = null, OutputParamVal = null, ExpectedResult = null, Link = null, OutputParam = null, ContentType = null, RequestType = null, ServiceRequest = null, RequestParameter = null, RequestHeader = null, ServiceResponse = null, ResponseHeader = null, Status = null, InputHeaderFromPreviousResponse = null, InputResponseFromPreviousResponse = null, Code = null, Time = null;

                DefaultTableModel Table_config = new DefaultTableModel();

                table = new JTable(Table_config);
                String[] header = {"Execute", "TestName", "RequestType", "ContentType", "ServiceRequest", "RequestParameter", "RequestHeader", "ServiceResponse", "ResponseHeader", "Status", "InputParam", "InputParamVal", "OutputParam", "OutputParamVal", "Link", "Code", "Time (ms)", "ExpectedResult"};
                DefaultTableModel model = new DefaultTableModel(new Object[][]{}, header);
                table = new JTable(model) {

                    private static final long serialVersionUID = 1L;

                    /*@Override
		            public Class getColumnClass(int column) {
		            return getValueAt(0, column).getClass();
		            }*/
                    @Override
                    public Class getColumnClass(int column) {
                        switch (column) {
                            case 0:
                                return Boolean.class;

                            default:
                                return String.class;
                        }
                    }
                };
                ////
                //table.setModel(new DefaultTableModel(new Object[][]{},header));
                int numCols = table.getModel().getColumnCount();

                table.addMouseListener(new java.awt.event.MouseAdapter() {

                    public void mouseClicked(java.awt.event.MouseEvent e) {

                        int row = table.rowAtPoint(e.getPoint());

                        int col = table.columnAtPoint(e.getPoint());

                        if (col != 0) {
                            //JTextArea tA = new JTextArea("<html><body><p style='width: 200px;'>" + table.getValueAt(row, col).toString() + "</p></body></html>");
                            JTextArea tA = new JTextArea(table.getValueAt(row, col).toString());
                            tA.setEditable(true);
                            tA.setDragEnabled(true);
                            JOptionPane.showMessageDialog(
                                    null,
                                    tA,
                                    table.getColumnName(col),
                                    JOptionPane.INFORMATION_MESSAGE);

                            //MessageDialogWithCopy.openInformation(null, table.getColumnName(col), table.getValueAt(row, col).toString());

                        }
                    }
                });

                table.setShowGrid(true);

                int height = 30;
                table.setRowHeight(height);


                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        //Boolean Execute=(boolean)eElement.getElementsByTagName("TestName").item(0).getTextContent();
                        Boolean Execute = true;
                        TestName = eElement.getElementsByTagName("TestName").item(0).getTextContent();
                        ContentType = eElement.getElementsByTagName("RequestType").item(0).getTextContent();
                        RequestType = eElement.getElementsByTagName("ContentType").item(0).getTextContent();
                        ServiceRequest = eElement.getElementsByTagName("ServiceRequest").item(0).getTextContent();
                        RequestParameter = eElement.getElementsByTagName("RequestParameter").item(0).getTextContent();
                        RequestHeader = eElement.getElementsByTagName("RequestHeader").item(0).getTextContent();
                        ServiceResponse = eElement.getElementsByTagName("ServiceResponse").item(0).getTextContent();
                        ResponseHeader = eElement.getElementsByTagName("ResponseHeader").item(0).getTextContent();
                        Status = eElement.getElementsByTagName("Status").item(0).getTextContent();
                        InputParam = eElement.getElementsByTagName("InputParam").item(0).getTextContent();
                        InputParamVal = eElement.getElementsByTagName("InputParamVal").item(0).getTextContent();
                        OutputParam = eElement.getElementsByTagName("OutputParam").item(0).getTextContent();
                        OutputParamVal = eElement.getElementsByTagName("OutputParamVal").item(0).getTextContent();
                        Link = eElement.getElementsByTagName("Link").item(0).getTextContent();
                        Code = eElement.getElementsByTagName("Code").item(0).getTextContent();
                        Time = eElement.getElementsByTagName("Time").item(0).getTextContent();
                        ExpectedResult = eElement.getElementsByTagName("ExpectedResult").item(0).getTextContent();


                        Object[] file = new Object[numCols];
                        file[0] = Boolean.FALSE;
                        file[1] = TestName;
                        file[2] = ContentType;
                        file[3] = RequestType;
                        file[4] = ServiceRequest;
                        file[5] = RequestParameter;
                        file[6] = RequestHeader;
                        file[7] = ServiceResponse;
                        file[8] = ResponseHeader;
                        file[9] = Status;
                        file[10] = InputParam;
                        file[11] = InputParamVal;
                        file[12] = OutputParam;
                        file[13] = OutputParamVal;
                        file[14] = Link;
                        file[15] = Code;
                        file[16] = Time;
                        file[17] = ExpectedResult;

                        ((DefaultTableModel) table.getModel()).addRow(file);

                        scrollPane.setViewportView(table);


                    }

                }

                ///////////////////////////////////////


            }
        });
        btnReload.setBounds(439, 66, 129, 23);
        frmServiceTestingTool.getContentPane().add(btnReload);

        JButton btnExportAsReport = new JButton("Export As Report");
        btnExportAsReport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {


                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                PrintWriter writer = null;

                try {
                    writer = new PrintWriter(System.getProperty("user.dir") + "/Report/Report-" + timeStamp + ".html", "UTF-8");
                    writer.println("<html><head><title>ServiceTestingReport</title></head>");
                    writer.println("<style type=\"text/css\">table.new { font: 11px/24px Verdana, Arial, Helvetica, sans-serif; border-collapse: collapse; overflow:hidden; word-wrap:break-word;  width: 500px;  } table.new th {padding: 0 0.5em; text-align: left;}tr.yellow table.new td { border-top: 1px solid #FB7A31; border-bottom: 1px solid #FB7A31; background: #FFC;}td { border-bottom: 1px solid #CCC; padding: 0 0.5em;vertical-align:top;}td:first-child { width: 50%; }td+td { border-left: 1px solid #CCC; text-align: left;  } table.new thead { background: #fc9; } table.new tbody { background: #D7FAFC; }</style><body bgcolor=\"7BBDF6\"><table align=center><tr><td><h1><u><font face=\"verdana\" color=\"FEFBFB\">Service Testing Report</font</u></h1></td></tr></table>");
                    writer.println("<table class=\"new\" align=center><thead><th>Test Description</th><th>Request Type</th><th>Content Type</th><th>Service Request</th><th>Request Parameter</th><th>Request Header</th><th>Service Response</th><th>Response Header</th><th>Status</th><th>Input Param</th><th>Input Param Value</th><th>Output Param</th><th>Output Param Val</th><th>Link</th><th>Code</th><th>Time</th><th>Expected Output</th></thread><tbody>");
                } catch (FileNotFoundException | UnsupportedEncodingException e1) {
                    //System.out.println("FileNotFound");
                }
                try {


                    try {
                        String newline = System.getProperty("line.separator");
                        try {


                            DefaultTableModel dtm = (DefaultTableModel) table.getModel();

                            int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();

                            for (int i = 0; i < nRow; i++) {

                                if (dtm.getValueAt(i, 0).toString().equalsIgnoreCase("True")) {
                                    //System.out.println("value of checkbox:"+dtm.getValueAt(i,0).toString());
                                    writer.println(newline + "<tr>");
                                    for (int j = 1; j < nCol; j++) {
                                        //if(j==2)
                                        //System.out.println("j="+j);
                                        //writer.println(newline+"<td>"+txtEnv.getText()+"."+dtm.getValueAt(i,j).toString().replaceAll("&", "&amp;")+"</td>");
                                        //else
                                        if (Objects.nonNull(dtm.getValueAt(i, j)))
                                            writer.println(newline + "<td>" + dtm.getValueAt(i, j).toString().replaceAll("&", "&amp;") + "</td>");
                                        else
                                            writer.println(newline + "<td></td>");
                                    }
                                    writer.println("</tr>" + newline);
                                }
                            }

                        } catch (Exception Ex) {
                            JOptionPane.showMessageDialog(null, "Unable to Save Table");
                            Ex.printStackTrace();
                        }


                    } catch (Exception Errr) {
                        //System.out.println("File not Found");
                    }


                } catch (Exception E) {
                    JOptionPane.showMessageDialog(null, "Found some issues.");
                    E.printStackTrace();

                }

                writer.println("</tbody></table></body></html>");
                writer.close();
                JOptionPane.showMessageDialog(null, "Report is generated at below location:\n" + System.getProperty("user.dir") + "/Report/Report-" + timeStamp + ".html");


                //////////////////////////


            }
        });
        btnExportAsReport.setFont(new Font("Arial", Font.PLAIN, 12));
        btnExportAsReport.setBounds(10, 66, 135, 23);
        frmServiceTestingTool.getContentPane().add(btnExportAsReport);

        JSeparator separator_1 = new JSeparator();
        separator_1.setBounds(10, 53, 1028, 2);
        frmServiceTestingTool.getContentPane().add(separator_1);

        JButton btnAddTestcase = new JButton("Add Testcase");
        btnAddTestcase.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NewTestCase newTest = new NewTestCase();
                newTest.newTestCase();
            }
        });

        btnAddTestcase.setFont(new Font("Arial", Font.PLAIN, 12));
        btnAddTestcase.setBounds(665, 10, 110, 23);
        frmServiceTestingTool.getContentPane().add(btnAddTestcase);

        //Add new workflow window
        JButton btnAddWorkflow = new JButton("Add Workflow");
        btnAddWorkflow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NewWorkflow newWorkflow = new NewWorkflow();
                newWorkflow.newWorkflow();
                if (newWorkflow.workflow_name != null) {
                    //workflowList[flowNum] = newWorkflow.workflow_name;
                    //flowNum++;
                }
            }


        });
        btnAddWorkflow.setFont(new Font("Arial", Font.PLAIN, 12));
        btnAddWorkflow.setBounds(1090, 475, 200, 23);
        frmServiceTestingTool.getContentPane().add(btnAddWorkflow);
        //
        //Update panel button
        JButton btnUpdatePanel = new JButton("Update Panel");
        btnUpdatePanel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                File directory = new File(System.getProperty("user.dir") + "/Workflows");

                // get all the files from a directory
                File[] fList = directory.listFiles();
                String[] Workflow_files = new String[fList.length];
                int count_files = 0;
                for (File file : fList) {

                    Workflow_files[count_files] = file.getName().toString().split(".xml")[0];
                    //model.addElement(file.getName().toString());
                    count_files++;
                }

                comboBox_1.setModel(new DefaultComboBoxModel(Workflow_files));
            }


        });
        btnUpdatePanel.setFont(new Font("Arial", Font.PLAIN, 12));
        btnUpdatePanel.setBounds(1090, 510, 200, 23);
        frmServiceTestingTool.getContentPane().add(btnUpdatePanel);
        //
        JButton btnViewReports = new JButton("View Reports");
        btnViewReports.setFont(new Font("Arial", Font.PLAIN, 12));
        btnViewReports.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Desktop desktop = Desktop.getDesktop();
                File dirToOpen = null;
                dirToOpen = new File(System.getProperty("user.dir") + "/Report");
                try {
                    desktop.open(dirToOpen);
                } catch (Exception e4) {
                    JOptionPane.showMessageDialog(null, "Report Folder is not present.");
                }
            }
        });
        btnViewReports.setBounds(928, 10, 110, 23);
        frmServiceTestingTool.getContentPane().add(btnViewReports);

        JButton btnExecuteAll = new JButton("Execute All");
        btnExecuteAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                URL url = null;
                lblNewLabel_1.setText("Please Wait.....");
                lblNewLabel_1.paintImmediately(lblNewLabel_1.getVisibleRect());
                //Check all code
                try {
                    ////System.out.println("ko:"+table.getRowCount());
                    for (int b = 0; b < table.getRowCount(); b++) {
                        table.getModel().setValueAt(true, b, 0);
                    }
                } catch (Exception er) {
                }
                //
                //New Code
                for (int b = 0; b < table.getRowCount(); b++) {

                    Boolean Execute = (Boolean) table.getModel().getValueAt(b, 0);
                    String Test_name = (String) table.getModel().getValueAt(b, 1);
                    String Request_Type = (String) table.getModel().getValueAt(b, 2);
                    String Content_Type = (String) table.getModel().getValueAt(b, 3);
                    String Service_Request1 = (String) table.getModel().getValueAt(b, 4);
                    String hostUrl = textField.getText();
                    String username = textField1.getText();
                    String password = textField2.getText();
                    String data_hostUrl = textField3.getText();
                    String data_username = textField4.getText();
                    String data_password = textField5.getText();
                    String portal_hostUrl = textField6.getText();
                    String portal_username = textField7.getText();
                    String portal_password = textField8.getText();
                    String prod_username = textField9.getText();
                    String prod_password = textField10.getText();
                    String Request_Header = "";
                    String Service_Request = comboBox.getSelectedItem() + Service_Request1;
                    if (Execute) {
                        if (Service_Request1.startsWith("http") )   {
                            Service_Request = Service_Request1;
                        }  if (Service_Request1.startsWith("jdbc")) {
                            Service_Request = Service_Request1;
                        }  if (Service_Request1.equalsIgnoreCase("%hostUrl%")) {
                            Service_Request = comboBox.getSelectedItem() + hostUrl;
                        }  if (Service_Request1.equalsIgnoreCase("%data_hostUrl%")) {
                            Service_Request = comboBox.getSelectedItem() + data_hostUrl;
                        }  if (Service_Request1.contains("%portal_environment%")) {
                            Service_Request = Service_Request1.replace("%portal_environment%", portal_hostUrl);
                        }

                        table.getModel().setValueAt(Service_Request, b, 4);
                        String Request_Parameter = (String) table.getModel().getValueAt(b, 5);
                        String val_req = Request_Parameter;
                        String status_val = table.getModel().getValueAt(b, 9).toString();


                        //
                        table.getModel().setValueAt(val_req, b, 5);
                        Request_Parameter = (String) table.getModel().getValueAt(b, 5);
                        //String Request_Parameter=comboBox.getSelectedItem()+" : "+Request_Parameter1;
                        //System.out.println("Service_Request="+Service_Request);
                        Request_Header = (String) table.getModel().getValueAt(b, 6);
                        if (Request_Header.equalsIgnoreCase("%credentials%") && !(Request_Header.startsWith("user"))) {
                            if (username!=null && password!=null)
                                Request_Header = "username:"+username+",password:"+password;
                            else
                                Request_Header = null;
                        } else if (Request_Header.equalsIgnoreCase("%data_credentials%") && !(Request_Header.startsWith("user"))) {
                            if (username!=null && password!=null)
                                Request_Header = "username:"+data_username+",password:"+data_password;
                            else
                                Request_Header = null;
                        } else if (Request_Header.equalsIgnoreCase("%portal_credentials%") && !(Request_Header.startsWith("user"))) {
                            if (username!=null && password!=null)
                                Request_Header = "username:"+portal_username+",password:"+portal_password;
                            else
                                Request_Header = null;
                        } else if (Request_Header.equalsIgnoreCase("%prod_credentials%") && !(Request_Header.startsWith("user"))) {
                            if (username!=null && password!=null)
                                Request_Header = "username:"+prod_username+",password:"+prod_password;
                            else
                                Request_Header = null;
                        }
                        table.getModel().setValueAt(Request_Header, b, 6);

                        if (Execute && Request_Type.equalsIgnoreCase("Trecvalidate")) {
                            try {
                                Trecvalidate(Test_name, Service_Request, Request_Header, b);

                                //Test Name
                                lblNewLabel.setText("Test Name" + " : " + table.getModel().getValueAt(table.getSelectedRow(), 1).toString());

                                //Service Request
                                textArea_1.setText(table.getModel().getValueAt(table.getSelectedRow(), 4).toString());

                                //Request Parameter
                                textArea_2.setText(table.getModel().getValueAt(table.getSelectedRow(), 5).toString());

                                //Request Header
                                textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 6).toString());

                                //Service Response
                                textArea_3.setText(table.getModel().getValueAt(table.getSelectedRow(), 7).toString());

                                //Response Header
                                textArea_4.setText(table.getModel().getValueAt(table.getSelectedRow(), 8).toString());


                            }
                            catch (Exception E)    {

                            }
                        }

                      
                        if (Execute && Request_Type.equalsIgnoreCase("DB_Validation")) {
                            //Select code
                            try {
                                dbValidation(Test_name, Service_Request, Request_Parameter, Request_Header, Content_Type, b);

                                //
                                lblNewLabel.setText("Test Name" + " : " + table.getModel().getValueAt(table.getSelectedRow(), 1).toString());
                                //Service Request
                                textArea_1.setText(table.getModel().getValueAt(table.getSelectedRow(), 4).toString());
                                //Request Parameter
                                textArea_2.setText(table.getModel().getValueAt(table.getSelectedRow(), 5).toString());
                                //Request Header
                                textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 6).toString());
                                //Service Response
                                textArea_3.setText(table.getModel().getValueAt(table.getSelectedRow(), 7).toString());
                                //REsponse Header
                                textArea_4.setText(table.getModel().getValueAt(table.getSelectedRow(), 8).toString());
                                //textArea.setText(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());
                            } catch (Exception E) {
                                //JOptionPane.showMessageDialog(null, "Please select a Test Case");
                            }
                        }

                    }


                }
                lblNewLabel_1.setText("Done");
                lblNewLabel_1.paintImmediately(lblNewLabel_1.getVisibleRect());
            }
        });
        btnExecuteAll.setFont(new Font("Arial", Font.PLAIN, 12));
        btnExecuteAll.setBounds(796, 10, 110, 23);
        frmServiceTestingTool.getContentPane().add(btnExecuteAll);

    }

    //DB Validation
    public void dbValidation(String testname, String dbUrl, String query, String credentials, String queryType, int row) throws Exception {


        query = query.replaceAll("&apos;", "\'");
        String inputParamValue = (String) table.getModel().getValueAt(row, 11);

        String username = null;
        String password = null;
        username = credentials.split("username:")[1].split(",")[0];
        if (credentials.contains("password:"))
            password = credentials.split("password:")[1].split(",")[0];
        String text = null;
        String Result = "FAIL";
        String newQuery = null;
        newQuery = query;
        String[] data = null;
        String endChar = "";
        ResultSet rs = null;
        ResultSet rs1 = null;
        String newQuery1 = "";
        String[] la_id = null;

        DBUtil dbObj = new DBUtil(dbUrl, username, password);

        // DATE MANIPULATION
        String s0 = "%_%months_back%_%";
        while (query.contains(s0)) {
            int index = query.indexOf(s0);
            int lastIndex = query.split(s0)[0].lastIndexOf("%");
            int firstIndex = query.split(s0)[1].indexOf("%");
            String array1[] = query.split(s0)[0].split(" %");
            int monthsBack = Integer.parseInt(array1[array1.length - 1]);
            int days = Integer.parseInt(query.split(s0)[1].split("% ")[0]);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = Date.from(ZonedDateTime.now().minusMonths(monthsBack).toInstant());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.DAY_OF_MONTH, days);
            java.sql.Date sd = new java.sql.Date(cal.getTime().getTime());
            //sdf.format(sd);
            String s1 = query.substring(0, lastIndex);
            String s2 = "\"" + sdf.format(sd) + "\"";
            String s3 = query.substring(index + s0.length() + firstIndex + 1);
            newQuery = s1 + s2 + s3;
            query = newQuery;
        }
        // DATE MANIPULATION
        s0 = "%_%months_forward%_%";
        while (query.contains(s0)) {
            int index = query.indexOf(s0);
            int lastIndex = query.split(s0)[0].lastIndexOf("%");
            int firstIndex = query.split(s0)[1].indexOf("%");
            String array1[] = query.split(s0)[0].split(" %");
            int monthsForward = Integer.parseInt(array1[array1.length - 1]);
            int days = Integer.parseInt(query.split(s0)[1].split("% ")[0]);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = Date.from(ZonedDateTime.now().plusMonths(monthsForward).toInstant());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.DAY_OF_MONTH, days);
            java.sql.Date sd = new java.sql.Date(cal.getTime().getTime());
            //sdf.format(sd);
            String s1 = query.substring(0, lastIndex);
            String s2 = "\"" + sdf.format(sd) + "\"";
            String s3 = query.substring(index + s0.length() + firstIndex + 1);
            newQuery = s1 + s2 + s3;
            query = newQuery;
        }

        //newQuery = query;


        //if readFromFile
        if (newQuery.contains("%readFromFile%")) {
            if (charCount(newQuery, '(') > charCount(newQuery, ')')) {
                int dif = charCount(newQuery, '(') - charCount(newQuery, ')');
                for (int m = 0; m < dif; m++) {
                    endChar += ")";
                }
            }
            //FileWriter fw = new FileWriter("databaseQueryResult.txt");
            //fw.flush();
            readfile fr = new readfile(System.getProperty("user.dir")+"/loanApplicationId.txt");
            data = fr.ReadFromFile().split(",");
            la_id = data;
            newQuery1 = "(\'" + data[0] + "\'";
            for (int dataCount = 1; dataCount < data.length; dataCount++) {
                newQuery1 += ",\'" + data[dataCount] + "\'";
            }
            newQuery1+=")";
            newQuery = query.replace("%readFromFile%", newQuery1) + endChar;
        }
        //if readPanNumber
        else if (newQuery.contains("%readPanNumber%")) {
            if (charCount(newQuery, '(') > charCount(newQuery, ')')) {
                int dif = charCount(newQuery, '(') - charCount(newQuery, ')');
                for (int m = 0; m < dif; m++) {
                    endChar += ")";
                }
            }
            readfile fr = new readfile(System.getProperty("user.dir")+"/panNumber.txt");
            data = fr.ReadFromFile().split(",");
            newQuery1 = "(\'" + data[0] + "\'";
            for (int dataCount = 1; dataCount < data.length; dataCount++)
                newQuery1 += ",\'" + data[dataCount] + "\'";
            newQuery1+=")";
            newQuery = query.replace("%readPanNumber%", newQuery1) + endChar;
        }
        //if readLan
        else if (newQuery.contains("%readLan%")) {
            if (charCount(newQuery, '(') > charCount(newQuery, ')')) {
                int dif = charCount(newQuery, '(') - charCount(newQuery, ')');
                for (int m = 0; m < dif; m++) {
                    endChar += ")";
                }
            }
            readfile fr = new readfile(System.getProperty("user.dir")+"/loanApplicationNumber.txt");
            data = fr.ReadFromFile().split(",");
            newQuery1 = "\'%" + data[0] + "\'";
            for (int dataCount = 1; dataCount < data.length; dataCount++)
                newQuery1 += ",\'" + data[dataCount] + "\'";
            newQuery1+="";
            newQuery = query.replace("%readLan%", newQuery1) + endChar;
        }
        //if read user_id_ref from previous service request
        else if (newQuery.contains("%readUserFromPrevious%")) {
            //if (newQuery.contains("("))
                //endChar = ")";
            String user_id_ref = inputParamValue.split("data:")[1].split(";")[0];
            newQuery = query.replace("%readUserFromPrevious%", user_id_ref);
            //newQuery += ")" + endChar;
            newQuery1 = "SELECT * FROM loan_application WHERE user_id_ref = "+user_id_ref+" AND user_data_review_status IN ('CLOSED') AND application_status IN ('CLOSED') ORDER BY date_modified DESC LIMIT 1";
            //query=newQuery;
            //table.getModel().setValueAt(newQuery, row, 5);
        }
        //if read loan_app_id from previous service request
        else if (newQuery.contains("%readLoanFromPrevious%")) {
            //if (newQuery.contains("("))
            //endChar = ")";
            String loan_app_id = null;
            if(inputParamValue.contains("data:"))
                loan_app_id = inputParamValue.split("data:")[1].split(";")[0];
            newQuery = query.replace("%readLoanFromPrevious%", loan_app_id);
            la_id[0] = loan_app_id;
            //newQuery += ")" + endChar;
            //newQuery1 = "SELECT * FROM loan_application WHERE user_id_ref = "+user_id_ref+" AND user_data_review_status IN ('CLOSED') AND application_status IN ('CLOSED') ORDER BY date_modified DESC LIMIT 1";
            //query=newQuery;
            //table.getModel().setValueAt(newQuery, row, 5);
        }

        if (testname.startsWith("amort_")) {
            if (newQuery.contains("%loanAppId%")) {
                newQuery.replaceAll("%loanAppId%", db_id);
            }
        }

        writefile fw = new writefile(System.getProperty("user.dir")+"/databaseQueryResult.txt");
        //writefile.writeToFile(newQuery+"\n");

        //BeforeTime
        Date d1 = new Date();


        if (queryType.equalsIgnoreCase("update") && newQuery.contains(" = %test")) {
            String tableName = newQuery.split("UPDATE ")[1].split(" SET")[0].trim();
            String[] updateColumns = newQuery.split("SET ")[1].split(" WHERE")[0].split(",");
            String queryCondition = newQuery.split("% WHERE ")[1];
            String readQuery = "SELECT * FROM "+tableName+" WHERE "+queryCondition;
            ResultSet rs2 = dbObj.selectQueryResultSet(readQuery);
            for (int i=0; i<updateColumns.length; i++)
            {
                boolean flag = false;
                String columnName = updateColumns[i].split("=")[0].trim();
                String columnValue = rs2.getString(columnName);
                if (columnValue==null) {
                    continue;
                }
                if(columnValue.contains("@") && (columnValue.contains("mail.com") || columnValue.contains("mail.in"))) {
                    columnValue = columnValue.replaceAll("@","test@");
                    flag = true;
                }
                if (columnValue.startsWith("+91")) {
                    if (!columnValue.endsWith("000")) {
                        columnValue = columnValue.substring(0, 10) + "000";
                        flag = true;
                    } else if (!columnValue.endsWith("999")) {
                        columnValue = columnValue.substring(0, 10) + "999";
                        flag = true;
                    }
                }
                if (flag==true) {
                    newQuery = newQuery.replace("%test" + columnName + "%", "\"" + columnValue + "\"");
                }
            }
        }

        if (queryType.equalsIgnoreCase("update") && query.contains("system_property")) {
            if (la_id==null) {
                readfile fr = new readfile(System.getProperty("user.dir")+"/loanApplicationId.txt");
                la_id = fr.ReadFromFile().split(",");
            }
            String updateColumn = "";
            String ids = "";
            String readQuery = null;
            readQuery = "SELECT value FROM system_property WHERE name IN ";
            if (testname.contains("_ficcl_"))
                updateColumn = "topup.ficcl.old.loan.ids.pos.mismatch.skip";
            else if (testname.contains("_dmi_"))
                updateColumn = "topup.dmi.old.loan.ids.pos.mismatch.skip";
            readQuery += "(\'" + updateColumn + "\')";
            ResultSet rs2 = dbObj.selectQueryResultSet(readQuery);
            String columnValue = rs2.getString("value");
            for (String id : la_id) {
                if (!columnValue.contains(id))
                    ids += "," + id;
            }
            newQuery = "UPDATE system_property SET value = \'" + columnValue +""+ ids + "\' WHERE name IN " + "(\'" + updateColumn + "\')";
        }

        if (queryType.equalsIgnoreCase("select")) {
            text = dbObj.selectQueryResult(newQuery);
            writefile.writeToFile("\n" + text + "\n");
            writefile.writeToFile("\n---END---");
        }

        else if (queryType.equalsIgnoreCase("insert") || queryType.equalsIgnoreCase("update")) {
            text = dbObj.updateQuery(newQuery);
            writefile.writeToFile("\n" + text + "\n");
            writefile.writeToFile("\n---END---");
        }

        else if (queryType.equalsIgnoreCase("delete")) {
            text = dbObj.deleteFromTable(newQuery);
            writefile.writeToFile("\n" + text + "\n");
            writefile.writeToFile("\n---END---");
        }

        else if (queryType.equalsIgnoreCase("copy")) {
            rsGlobal = dbObj.copyQueryResult(newQuery);
            //db_id = rs.getString("id");
            //writefile.writeToFile(text,fw);
        }

        else if (queryType.equalsIgnoreCase("paste")) {
            text = dbObj.pasteQueryResult(newQuery, rsGlobal);
            writefile.writeToFile("\n" + text + "\n");
            writefile.writeToFile("\n---END---");
            rs = null;
        }

        //fw.close();

        //AfterTime
        Date d2 = new Date();

        //TimeDifference
        String timeDiff = null;
        try {
            long diff = d2.getTime() - d1.getTime();
            int ss = (int) (diff / 1000);
            int mm = (int) (diff / 60000);
            int hh = (int) (diff / 3600000);
            String sec = "" + ss, min = "" + mm, hour = "" + hh;
            if (ss > 59) {
                ss = 0;
                mm++;
            }
            if (ss < 10) {
                sec = "0" + ss;
            }
            if (mm > 59) {
                mm = 0;
                hh++;
            }
            if (mm < 10) {
                min = "0" + mm;
            }
            if (hh < 10) {
                hour = "0" + hh;
            }
            //timeDiff = hour + ":" + min + ":" + sec;
            timeDiff = Long.valueOf(diff).toString();
            System.out.println("Response Time Display = " + timeDiff + "\n");
        } catch (Exception e) {
            timeDiff = "Error";
        }

        table.getModel().setValueAt(dbUrl, row, 4);
        table.getModel().setValueAt(newQuery, row, 5);
        table.getModel().setValueAt(text, row, 7);

        //table.getModel().setValueAt(response.toString(), row, 8);

        //Code for pass fail
        String actualOutput = (String) table.getModel().getValueAt(row, 7);
        //System.out.println("check actualoutput="+actualOutput);
        String expectedOutput = (String) table.getModel().getValueAt(row, 17);
        //System.out.println("check expectedoutput="+expectedOutput);
        if(!testname.contains("_checkPostHappyPath"))
            Result = statusReport(actualOutput, expectedOutput);

        table.getModel().setValueAt(Result, row, 9);
        table.getModel().setValueAt("Done", row, 15);
        table.getModel().setValueAt(timeDiff, row, 16);
    }

    // TREC Validation
    public void Trecvalidate(String testname, String url, String credentials, int row) {

        String Result = "FAIL";
        String[] data = null;

        String username = null;
        String password = null;
        username = credentials.split("username:")[1].split(",")[0];
        if (credentials.contains("password:"))
            password = credentials.split("password:")[1].split(",")[0];

        //if readFromFile
        if (url.contains("%readFromFile%")) {
            readfile fr = new readfile(System.getProperty("user.dir")+"/loanApplicationId.txt");
            data = fr.ReadFromFile().split(",");
            url = url.replace("%readFromFile%", data[0]);
        }

        //BeforeTime
        Date d1 = new Date();

        try {

            //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/chromedriver");

            // Initialize browser
            WebDriver driver = new ChromeDriver();

            JavascriptExecutor js = (JavascriptExecutor) driver;

            // Open Portal
                        driver.navigate().to(url);
                        driver.manage().window().maximize();
                        Thread.sleep(500);

            // Enter Username
                        WebElement username1 = driver.findElement(By.xpath("//input[@type='text' and contains(@id,'username')]"));
                        username1.click();
                        username1.clear();
                        username1.sendKeys(username);

            // Enter Password
                        WebElement password1 = driver.findElement(By.xpath("//input[@type='password' and contains(@id,'password')]"));
                        password1.click();
                        password1.clear();
                        password1.sendKeys(password);
                        Thread.sleep(500);

            // Click on Login button
                        WebElement loginBtn = driver.findElement(By.xpath("//button[@type='submit']"));
                        loginBtn.click();
                        Thread.sleep(500);

            // Reprocess from portal
            if (testname.contains("_reprocess")) {
                try {
                    //This will scroll the web page till end.
                    js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
                    
                    if (driver.findElement(By.xpath("//input[@id='reprocessPayments' and @type='button']")).isEnabled()) {
                        driver.findElement(By.xpath("//input[@id='reprocessPayments' and @type='button']")).click();
                        Thread.sleep(10000);
                        String alertText = driver.switchTo().alert().getText();
                        if (alertText.contains("Successfully processed the payments")) {
                            Result = "PASS";
                            driver.switchTo().alert().accept();
                        }
                        //System.out.println("Click Pass");
                    }
                    driver.quit();
                } catch (Exception ee) {
                    System.out.println("inside catch of isEnabled");
                    ee.printStackTrace();
                }
            }

            //AfterTime
            Date d2 = new Date();

            //TimeDifference
            String timeDiff = null;
            try {
                long diff = d2.getTime() - d1.getTime();
                int ss = (int) (diff / 1000);
                int mm = (int) (diff / 60000);
                int hh = (int) (diff / 3600000);
                String sec = "" + ss, min = "" + mm, hour = "" + hh;
                if (ss > 59) {
                    ss = 0;
                    mm++;
                }
                if (ss < 10) {
                    sec = "0" + ss;
                }
                if (mm > 59) {
                    mm = 0;
                    hh++;
                }
                if (mm < 10) {
                    min = "0" + mm;
                }
                if (hh < 10) {
                    hour = "0" + hh;
                }
                //timeDiff = hour + ":" + min + ":" + sec;
                timeDiff = Long.valueOf(diff).toString();
                System.out.println("Response Time Display = " + timeDiff + "\n");
            } catch (Exception e) {
                timeDiff = "Error";
            }

            table.getModel().setValueAt(url, row, 4);
            table.getModel().setValueAt(Result, row, 9);
            table.getModel().setValueAt("Done", row, 15);
            table.getModel().setValueAt(timeDiff, row, 16);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Function to count the number of occurences of a character in a string
    public int charCount(String text1, char c) {
        int count = 0;
        for (int i = 0; i<text1.length(); i++) {
            if (text1.charAt(i)==c) {
                count++;
            }
        }
        return count;
    }
}