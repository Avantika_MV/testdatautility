import org.w3c.dom.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;


public class NewWorkflow {

    JTextArea textArea_SelectedTestCases = new JTextArea();
    String workflow_name = null;
    private JFrame frmAddNewTest;
    private JTextField txtTestDescription;
    private JTextField textField;
    private JTextField txtNa;
    private JTextField txtNa_1;
    private JTextField txtNa_2;
    private JTable table;

    /**
     * Create the application.
     */
    public NewWorkflow() {
        initialize();
    }

    /**
     * Launch the application.
     */
    public static void newWorkflow() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    NewWorkflow window = new NewWorkflow();
                    window.frmAddNewTest.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmAddNewTest = new JFrame();
        frmAddNewTest.setTitle("Add New Workflow");
        frmAddNewTest.setBounds(175, 10, 989, 700);
        frmAddNewTest.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frmAddNewTest.getContentPane().setLayout(null);

        final JScrollPane scrollPane = new JScrollPane();
        JScrollPane scrollPane1 = new JScrollPane();

        // Adding workflow name
        JLabel lblWorkflowName = new JLabel("Worflow name");
        lblWorkflowName.setBounds(10, 11, 100, 20);
        frmAddNewTest.getContentPane().add(lblWorkflowName);

        //text area for workflow name
        txtTestDescription = new JTextField();
        txtTestDescription.setBounds(120, 11, 400, 20);
        frmAddNewTest.getContentPane().add(txtTestDescription);
        txtTestDescription.setColumns(10);
        workflow_name = txtTestDescription.getText();

        // Check All Button
        JButton btnCheckAll = new JButton("Check All");
        btnCheckAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    //System.out.println("ko:"+table.getRowCount());
                    for (int b = 0; b < table.getRowCount(); b++) {
                        table.getModel().setValueAt(true, b, 0);
                    }
                } catch (Exception er) {
                }
            }
        });
        btnCheckAll.setFont(new Font("Arial", Font.PLAIN, 12));
        btnCheckAll.setBounds(10, 45, 101, 25);
        frmAddNewTest.getContentPane().add(btnCheckAll);
        //
        //UnCheck all button
        JButton btnUnCheckAll = new JButton("UnCheck All");
        btnUnCheckAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    //System.out.println("ko:"+table.getRowCount());
                    for (int b = 0; b < table.getRowCount(); b++) {
                        table.getModel().setValueAt(false, b, 0);
                    }
                } catch (Exception er) {
                }
            }
        });
        btnUnCheckAll.setFont(new Font("Arial", Font.PLAIN, 12));
        btnUnCheckAll.setBounds(140, 45, 101, 25);
        frmAddNewTest.getContentPane().add(btnUnCheckAll);

        //Move up button
        JButton btnMoveUp = new JButton("Move Up");
        btnMoveUp.setFont(new Font("Arial", Font.PLAIN, 12));
        btnMoveUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    DefaultTableModel model = (DefaultTableModel) table.getModel();

                    int[] rows = table.getSelectedRows();
                    model.moveRow(rows[0], rows[rows.length - 1], rows[0] - 1);
                    table.setRowSelectionInterval(rows[0] - 1, rows[rows.length - 1] - 1);
                } catch (Exception E2) {

                }

            }
        });
        btnMoveUp.setBounds(270, 45, 89, 23);
        frmAddNewTest.getContentPane().add(btnMoveUp);

        //Move down button
        JButton btnMoveDown = new JButton("Move Down");
        btnMoveDown.setFont(new Font("Arial", Font.PLAIN, 12));
        btnMoveDown.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    DefaultTableModel model = (DefaultTableModel) table.getModel();
                    int[] rows = table.getSelectedRows();
                    model.moveRow(rows[0], rows[rows.length - 1], rows[0] + 1);
                    table.setRowSelectionInterval(rows[0] + 1, rows[rows.length - 1] + 1);
                } catch (Exception e3) {

                }

            }
        });
        btnMoveDown.setBounds(400, 45, 101, 23);
        frmAddNewTest.getContentPane().add(btnMoveDown);
        //
        //Refresh button
        JButton btnReload = new JButton("Refresh");
        btnReload.setFont(new Font("Arial", Font.PLAIN, 12));
        btnReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                table = new JTable();
                scrollPane.setViewportView(table);


                //////////////////////////////////
                File path = new File(System.getProperty("user.dir")+"/TestCases/TestCases1.xml");
                XML_Reader xmlInput = new XML_Reader();
                NodeList nList = xmlInput.readXML(path, "testcase");
                String TestName = null;
                DefaultTableModel Table_config = new DefaultTableModel();

                table = new JTable(Table_config);
                String[] header = {"Execute", "TestName"};
                DefaultTableModel model = new DefaultTableModel(new Object[][]{}, header);
                table = new JTable(model) {

                    private static final long serialVersionUID = 1L;


                    @Override
                    public Class getColumnClass(int column) {
                        switch (column) {
                            case 0:
                                return Boolean.class;

                            default:
                                return String.class;
                        }
                    }
                };
                ////
                //table.setModel(new DefaultTableModel(new Object[][]{},header));
                int numCols = table.getModel().getColumnCount();


                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        //Boolean Execute=(boolean)eElement.getElementsByTagName("TestName").item(0).getTextContent();
                        Boolean Execute = true;
                        TestName = eElement.getElementsByTagName("TestName").item(0).getTextContent();

                        Object[] file = new Object[numCols];
                        file[0] = Boolean.FALSE;
                        file[1] = TestName;


                        ((DefaultTableModel) table.getModel()).addRow(file);


                        scrollPane.setViewportView(table);


                    }

                }
                textArea_SelectedTestCases.setText("");

            }
        });
        btnReload.setBounds(530, 45, 101, 23);
        frmAddNewTest.getContentPane().add(btnReload);
        //

        //Select test cases

        JLabel lblSelectTestCases = new JLabel("Select Test Cases");
        lblSelectTestCases.setBounds(10, 70, 200, 80);
        frmAddNewTest.getContentPane().add(lblSelectTestCases);
        //

        //displays all the test cases with execute checkbox

        scrollPane.setBounds(22, 140, 300, 300);
        frmAddNewTest.getContentPane().add(scrollPane);

        table = new JTable();
        scrollPane.setViewportView(table);


        File path = new File(System.getProperty("user.dir")+"/TestCases/TestCases1.xml");
        XML_Reader xmlInput = new XML_Reader();
        NodeList nList = xmlInput.readXML(path, "testcase");
        String TestName = null;
        DefaultTableModel Table_config = new DefaultTableModel();

        table = new JTable(Table_config);
        String[] header = {"Execute", "TestName"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, header);
        table = new JTable(model) {

            private static final long serialVersionUID = 1L;

            /*@Override
                    public Class getColumnClass(int column) {
                    return getValueAt(0, column).getClass();
                    }*/
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Boolean.class;

                    default:
                        return String.class;
                }
            }
        };
        int numCols = table.getModel().getColumnCount();

        //Reads value from xml
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                Boolean Execute = true;
                TestName = eElement.getElementsByTagName("TestName").item(0).getTextContent();

                Object[] file = new Object[numCols];

                file[0] = Boolean.FALSE;
                file[1] = TestName;


                ((DefaultTableModel) table.getModel()).addRow(file);

                scrollPane.setViewportView(table);
            }

        }
        //
        //Selected Test case

        JLabel lblSelectedTestCases = new JLabel("Selected Test Cases");
        lblSelectedTestCases.setBounds(600, 70, 200, 80);
        frmAddNewTest.getContentPane().add(lblSelectedTestCases);
        //
        //Displays the selected test cases
        scrollPane1.setBounds(600, 140, 300, 300);
        frmAddNewTest.getContentPane().add(scrollPane1);

        textArea_SelectedTestCases.setLineWrap(true);
        scrollPane1.setViewportView(textArea_SelectedTestCases);

        //
        // Select button

        JButton btnSelect = new JButton("Select");
        btnSelect.setFont(new Font("Arial", Font.PLAIN, 12));

        btnSelect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String existingText = "";
                try {
                    for (int b = 0; b < table.getRowCount(); b++) {
                        Boolean Select_Execute = (Boolean) table.getModel().getValueAt(b, 0);
                        String Select_Test_name = (String) table.getModel().getValueAt(b, 1);
                        if (Select_Execute) {
                            existingText = existingText + "\n" + Select_Test_name;
                            textArea_SelectedTestCases.setText(existingText);

                        } else {

                            continue;
                        }

                    }
                } catch (Exception e3) {

                }

            }
        });
        btnSelect.setBounds(100, 470, 100, 30);
        frmAddNewTest.getContentPane().add(btnSelect);
        //
        //Save button
        JButton btnSave = new JButton("Save");
        btnSave.setFont(new Font("Arial", Font.PLAIN, 12));

        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {

                    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

                    // root elements
                    Document doc = docBuilder.newDocument();
                    Element rootElement = doc.createElement("data");
                    doc.appendChild(rootElement);

                    // staff elements
                    Element staff = doc.createElement("testcase");
                    rootElement.appendChild(staff);

                    // shorten way
                    // staff.setAttribute("id", "1");

                    // write the content into xml file
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();

                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");

                    DOMSource source = new DOMSource(doc);
                    String workflow_name = txtTestDescription.getText();
                    System.out.println(workflow_name);

                    //StreamResult result = new StreamResult(new File("user.dir")+"\\TestCases\\"+workflow_name+".xml");
                    System.out.println(("user.dir") + "/Workflows/" + workflow_name + ".xml");
                    StreamResult result = new StreamResult(new File(System.getProperty("user.dir")+"/Workflows/" + workflow_name) + ".xml");

                    String SelectedTestCases = textArea_SelectedTestCases.getText().trim();
                    //System.out.println("SelectedTestCases="+SelectedTestCases);
                    String arr_TestCases[] = SelectedTestCases.split("\n");


                    for (int i = 0; i < arr_TestCases.length; i++) {
                        //Element TestName = doc.createElement("TestName"+i);
                        //TestName.appendChild(doc.createTextNode(arr_TestCases[i]));
                        //staff.appendChild(TestName);


                        Element TestName = doc.createElement("TestName");
                        TestName.appendChild(doc.createTextNode(arr_TestCases[i]));
                        staff.appendChild(TestName);

                        // set attribute to staff element
                        Attr attr = doc.createAttribute("id");
                        String count = "" + i;
                        attr.setValue(count);
                        TestName.setAttributeNode(attr);


                    }


                    transformer.transform(source, result);

                    System.out.println("File saved!");

                } catch (ParserConfigurationException pce) {
                    pce.printStackTrace();
                } catch (TransformerException tfe) {
                    tfe.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "Workflow is Saved");
            }
        });
        btnSave.setBounds(700, 470, 100, 30);
        frmAddNewTest.getContentPane().add(btnSave);
        //
    }
}