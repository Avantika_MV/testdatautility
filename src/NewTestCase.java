import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class NewTestCase {

    private JFrame frmAddNewTest;
    private JTextField txtTestDescription;
    private JTextField textField;
    private JTextField txtNa;
    private JTextField txtNa_1;
    private JTextField txtNa_2;
    private JTable table;

    /**
     * Create the application.
     */
    public NewTestCase() {
        initialize();
    }

    /**
     * Launch the application.
     */
    public static void newTestCase() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    NewTestCase window = new NewTestCase();
                    window.frmAddNewTest.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmAddNewTest = new JFrame();
        frmAddNewTest.setTitle("Add New Test Case");
        frmAddNewTest.setBounds(175, 10, 989, 700);
        frmAddNewTest.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frmAddNewTest.getContentPane().setLayout(null);

        txtTestDescription = new JTextField();
        txtTestDescription.setText("Please Provide Test Description Here");
        txtTestDescription.setBounds(10, 11, 953, 20);
        frmAddNewTest.getContentPane().add(txtTestDescription);
        txtTestDescription.setColumns(10);

        JLabel lblRequestType = new JLabel("Request Type");
        lblRequestType.setBounds(10, 45, 101, 14);
        frmAddNewTest.getContentPane().add(lblRequestType);

        final JComboBox comboBox = new JComboBox();
        comboBox.setModel(new DefaultComboBoxModel(new String[]{"POST", "GET", "UI_Validation", "DB_Validation", "email_validation"}));
        comboBox.setBounds(121, 42, 119, 20);
        frmAddNewTest.getContentPane().add(comboBox);

		/*
		//Set
				JComboBox comboBox1 = new JComboBox();
				comboBox.setModel(new DefaultComboBoxModel(new String[] {"https://rqa1", "https://fqax"}));
				comboBox.setBounds(140, 11, 148, 20);
				frmAddNewTest.getContentPane().add(comboBox1);
				//txtEnv.setColumns(10);
				//

				JLabel lblEnvironment = new JLabel("Environment/Domain");
				lblEnvironment.setBounds(10, 14, 157, 14);
				frmAddNewTest.getContentPane().add(lblEnvironment);
		 */

        JLabel lblServiceRequest = new JLabel("Service Request End Point");
        lblServiceRequest.setBounds(264, 45, 167, 14);
        frmAddNewTest.getContentPane().add(lblServiceRequest);

        textField = new JTextField();
        textField.setBounds(421, 42, 275, 20);
        frmAddNewTest.getContentPane().add(textField);
        textField.setColumns(10);

        JLabel lblContentType = new JLabel("Content Type");
        lblContentType.setBounds(724, 45, 75, 14);
        frmAddNewTest.getContentPane().add(lblContentType);

        final JComboBox comboBox_1 = new JComboBox();
        comboBox_1.setModel(new DefaultComboBoxModel(new String[]{"application/json", "application/xml", "application/atom+xml", "application/javascript", "text/plain", "text/css", "text/html"}));
        comboBox_1.setBounds(824, 42, 138, 20);
        frmAddNewTest.getContentPane().add(comboBox_1);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(10, 94, 953, 123);
        frmAddNewTest.getContentPane().add(scrollPane_1);

        final JTextArea textArea = new JTextArea();
        textArea.setLineWrap(true);
        scrollPane_1.setViewportView(textArea);

        JLabel lblRequestParameter = new JLabel("Request Parameter");
        lblRequestParameter.setBounds(10, 79, 119, 14);
        frmAddNewTest.getContentPane().add(lblRequestParameter);

        JScrollPane scrollPane_2 = new JScrollPane();
        scrollPane_2.setBounds(10, 253, 953, 48);
        frmAddNewTest.getContentPane().add(scrollPane_2);

        final JTextArea txtrNa = new JTextArea();
        scrollPane_2.setViewportView(txtrNa);
        txtrNa.setText("N/A");

        JLabel lblRequestHeader = new JLabel("Request Header");
        lblRequestHeader.setBounds(10, 228, 147, 14);
        frmAddNewTest.getContentPane().add(lblRequestHeader);

        JLabel lblInputParameterFrom = new JLabel("Input Parameter From Previous Response (Comma seperated field name)");
        lblInputParameterFrom.setBounds(10, 304, 676, 14);
        frmAddNewTest.getContentPane().add(lblInputParameterFrom);

        txtNa = new JTextField();
        txtNa.setFont(new Font("Monospaced", Font.PLAIN, 11));
        txtNa.setText("N/A");
        txtNa.setBounds(10, 321, 953, 20);
        frmAddNewTest.getContentPane().add(txtNa);
        txtNa.setColumns(10);

        JLabel lblInputHeaderFrom = new JLabel("Input Header From Previous Response Header (Comma seperated field name)");
        lblInputHeaderFrom.setBounds(10, 342, 692, 14);
        frmAddNewTest.getContentPane().add(lblInputHeaderFrom);

        txtNa_1 = new JTextField();
        txtNa_1.setFont(new Font("Monospaced", Font.PLAIN, 11));
        txtNa_1.setText("N/A");
        txtNa_1.setBounds(10, 361, 953, 20);
        frmAddNewTest.getContentPane().add(txtNa_1);
        txtNa_1.setColumns(10);

        JLabel lblOutputParameterFor = new JLabel("Output Parameter For Next Response (Comma seperated field name)");
        lblOutputParameterFor.setBounds(10, 381, 708, 14);
        frmAddNewTest.getContentPane().add(lblOutputParameterFor);

        txtNa_2 = new JTextField();
        txtNa_2.setFont(new Font("Monospaced", Font.PLAIN, 11));
        txtNa_2.setText("N/A");
        txtNa_2.setBounds(10, 399, 953, 20);
        frmAddNewTest.getContentPane().add(txtNa_2);
        txtNa_2.setColumns(10);

        JButton btnAdd = new JButton("ADD");
        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int numCols = table.getModel().getColumnCount();
                Object[] file = new Object[numCols];
                file[1] = txtTestDescription.getText();
                file[2] = comboBox.getSelectedItem();
                file[3] = comboBox_1.getSelectedItem();
                file[4] = textField.getText();
                file[6] = txtrNa.getText();
                file[5] = textArea.getText();
                file[7] = "N/A";
                file[8] = txtNa_1.getText();
                file[9] = "N/A";
                file[10] = txtNa.getText();
                file[11] = "N/A";
                file[12] = txtNa_2.getText();
                file[13] = "N/A";
                file[14] = "N/A";
                file[15] = "N/A";
                file[16] = "N/A";
                file[17] = "N/A";
                ((DefaultTableModel) table.getModel()).addRow(file);


            }
        });
        btnAdd.setBounds(874, 430, 89, 23);
        frmAddNewTest.getContentPane().add(btnAdd);


        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(22, 473, 941, 134);
        frmAddNewTest.getContentPane().add(scrollPane);


        table = new JTable();
        scrollPane.setViewportView(table);


        File path = new File(System.getProperty("user.dir")+"/TestCases/TestCases1.xml");
        XML_Reader xmlInput = new XML_Reader();
        NodeList nList = xmlInput.readXML(path, "testcase");
        String TestName = null, InputParam = null, InputParamVal = null, OutputParamVal = null, Link = null, OutputParam = null, ExpectedResult = null, ContentType = null, RequestType = null, ServiceRequest = null, RequestParameter = null, RequestHeader = null, ServiceResponse = null, ResponseHeader = null, Status = null, InputHeaderFromPreviousResponse = null, InputResponseFromPreviousResponse = null, Code = null, Time = null;
        DefaultTableModel Table_config = new DefaultTableModel();

        //table = new JTable(Table_config);
        //table.setModel(new DefaultTableModel(
        //new Object[][]{},new String[]{"Execute","TestName", "RequestType","ContentType", "ServiceRequest", "RequestParameter", "RequestHeader","ServiceResponse","ResponseHeader","Status","InputParam","InputParamVal","OutputParam","OutputParamVal","Link"}));

        //

        table = new JTable(Table_config);
        String[] header = {"Execute", "TestName", "RequestType", "ContentType", "ServiceRequest", "RequestParameter", "RequestHeader", "ServiceResponse", "ResponseHeader", "Status", "InputParam", "InputParamVal", "OutputParam", "OutputParamVal", "Link", "Code", "Time (ms)", "ExpectedResult"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, header);
        table = new JTable(model) {

            private static final long serialVersionUID = 1L;

            /*@Override
            public Class getColumnClass(int column) {
            return getValueAt(0, column).getClass();
            }*/
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Boolean.class;

                    default:
                        return String.class;
                }
            }
        };
        //

        int numCols = table.getModel().getColumnCount();


        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                Boolean Execute = true;
                TestName = eElement.getElementsByTagName("TestName").item(0).getTextContent();
                ContentType = eElement.getElementsByTagName("RequestType").item(0).getTextContent();
                RequestType = eElement.getElementsByTagName("ContentType").item(0).getTextContent();
                ServiceRequest = eElement.getElementsByTagName("ServiceRequest").item(0).getTextContent();
                RequestParameter = eElement.getElementsByTagName("RequestParameter").item(0).getTextContent();
                RequestHeader = eElement.getElementsByTagName("RequestHeader").item(0).getTextContent();
                ServiceResponse = eElement.getElementsByTagName("ServiceResponse").item(0).getTextContent();
                ResponseHeader = eElement.getElementsByTagName("ResponseHeader").item(0).getTextContent();
                Status = eElement.getElementsByTagName("Status").item(0).getTextContent();
                InputParam = eElement.getElementsByTagName("InputParam").item(0).getTextContent();
                InputParamVal = eElement.getElementsByTagName("InputParamVal").item(0).getTextContent();
                OutputParam = eElement.getElementsByTagName("OutputParam").item(0).getTextContent();
                OutputParamVal = eElement.getElementsByTagName("OutputParamVal").item(0).getTextContent();
                Link = eElement.getElementsByTagName("Link").item(0).getTextContent();
                Code = eElement.getElementsByTagName("Code").item(0).getTextContent();
                Time = eElement.getElementsByTagName("Time").item(0).getTextContent();
                ExpectedResult = eElement.getElementsByTagName("ExpectedResult").item(0).getTextContent();


                Object[] file = new Object[numCols];

                file[0] = Boolean.FALSE;
                file[1] = TestName;
                file[2] = ContentType;
                file[3] = RequestType;
                file[4] = ServiceRequest;
                file[5] = RequestParameter;
                file[6] = RequestHeader;
                file[7] = ServiceResponse;
                file[8] = ResponseHeader;
                file[9] = Status;
                file[10] = InputParam;
                file[11] = InputParamVal;
                file[12] = OutputParam;
                file[13] = OutputParamVal;
                file[14] = Link;
                file[15] = Code;
                file[16] = Time;
                file[17] = ExpectedResult;

                ((DefaultTableModel) table.getModel()).addRow(file);

                scrollPane.setViewportView(table);
            }

        }


        JButton btnSave = new JButton("SAVE");
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {


                Writer xml_file = null;
                File statText = null;
                FileOutputStream is = null;
                OutputStreamWriter osw = null;
                String newline = System.getProperty("line.separator");
                try {
                    statText = new File(System.getProperty("user.dir")+"/TestCases/TestCases1.xml");
                    System.out.println("opened the file");
                    is = new FileOutputStream(statText);
                    osw = new OutputStreamWriter(is, "UTF8");
                    xml_file = new BufferedWriter(osw);
                    DefaultTableModel dtm = (DefaultTableModel) table.getModel();
                    int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
                    xml_file.write("<?xml version=\"1.0\"?>" + newline + "<data>");
                    for (int i = 0; i < nRow; i++) {
                        System.out.println("inside for loop of add test case");
                        xml_file.write(newline + "<testcase>");
                        for (int j = 1; j < nCol; j++) {
                            //xml_file.write(newline);"<"+table.getModel().getColumnName(j)+">"+dtm.getValueAt(i,j).toString().replaceAll("&", "&amp;")+"</"+table.getModel().getColumnName(j)+">");
                            xml_file.write(newline);
                            String ColumnName = table.getModel().getColumnName(j);
                            xml_file.write("<" + ColumnName + ">");
                            System.out.println(ColumnName);
                            String value = dtm.getValueAt(i, j).toString();
                            xml_file.write(value.replaceAll("&", "&amp;"));
                            System.out.println("testname=" + value);
                            //..replaceAll("&","&amp;"));
                            xml_file.write("</" + ColumnName + ">");
                        }
                        xml_file.write(newline + "</testcase>");
                    }
                    xml_file.write(newline + "</data>");
                    xml_file.close();
                    JOptionPane.showMessageDialog(null, "Saved The Table");
                } catch (Exception Ex) {
                    JOptionPane.showMessageDialog(null, "Unable to Save Table");
                    System.out.println(Ex);
                }


            }
        });
        btnSave.setBounds(874, 618, 89, 23);
        frmAddNewTest.getContentPane().add(btnSave);
    }
}