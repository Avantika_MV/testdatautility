import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class readfile {

    static String fileName;

    @SuppressWarnings("static-access")
    public readfile(String fileName) {
        super();
        this.fileName = fileName;
    }

    /*
    public static void main(String[] args) throws IOException {
        String textinfile=ReadFromFile();
        System.out.println("textinfile="+textinfile);

    }*/
    public String ReadFromFile() {
        String FILENAME = fileName;
        if (FILENAME.length() == 0)
            FILENAME = "dummy.txt";
        BufferedReader br = null;
        FileReader fr = null;
        String sCurrentLine = "";
        String textinfile = "";

        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);


            while ((sCurrentLine = br.readLine()) != null) {
                //System.out.println(sCurrentLine);
                textinfile += sCurrentLine+",";
            }
            fr.close();

        } catch (IOException e) {

            e.printStackTrace();

        }
        System.out.println("Read Success...");
        System.out.println("return=" + textinfile);
        return textinfile;
    }


}