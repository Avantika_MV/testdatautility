
import java.sql.*;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class DBUtil {

    private static Connection connection;

    public DBUtil(String DB_URL, String DB_User, String DB_Password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(DB_URL, DB_User, DB_Password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String selectQueryResult(String query) {
        ResultSet rs = null;
        String text = "";
        try {

                Statement stmt = connection.createStatement();
                //TestLog.stepInfo("Firing select query :" + query);
            //writefile fw = new writefile("databaseQueryResult.txt");

            rs = stmt.executeQuery(query);
            writefile.writeToFile("\n"+query+"\n");


                    //System.out.println("current " + tableName + " is:");
                    try {
                        if (!rs.isBeforeFirst()) {
                            System.out.println("no rows found");
                        }
                        else {
                            while (rs.next()) {
                                for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
                                    text+=" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i) + ",";
                                    System.out.print(" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i));
                                }
                                text+="\n";
                                //writefile.writeToFile(text,fw);
                                System.out.println();
                            }

                        }
                        //TestLog.stepInfo("Select query has been successfully executed");
            } catch (Exception e) {
                //TestLog.stepInfo("Problem in select query, exception " + e.getMessage());

                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    public ResultSet selectQueryResultSet(String query) {
        ResultSet rs = null;
        String text = "";
        try {

            Statement stmt = connection.createStatement();
            //TestLog.stepInfo("Firing select query :" + query);
            //writefile fw = new writefile("databaseQueryResult.txt");

            rs = stmt.executeQuery(query);
            writefile.writeToFile("\n"+query+"\n");


            //System.out.println("current " + tableName + " is:");
            try {
                if (!rs.isBeforeFirst()) {
                    System.out.println("no rows found");
                }
                else {
                    while (rs.next()) {
                        for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
                            text+=" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i) + ",";
                            System.out.print(" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i));
                        }
                        text+="\n";
                        //writefile.writeToFile(text,fw);
                        System.out.println();
                        return rs;
                    }

                }
                //TestLog.stepInfo("Select query has been successfully executed");
            } catch (Exception e) {
                //TestLog.stepInfo("Problem in select query, exception " + e.getMessage());

                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public String updateQuery(String query) {
        String flag = "failure";
        try {
            try {
                //TestLog.stepInfo("Firing update query :" + query);
                Statement stmt = connection.createStatement();
                stmt.executeUpdate(query);
                //writefile fw = new writefile("databaseQueryResult.txt");
                writefile.writeToFile("\n"+query+"\n");
                flag = "success";
                //TestLog.stepInfo("Update query has been done successfully executed");
            } catch (Exception e) {
                //TestLog.stepInfo("Problem in update query, exception " + e.getMessage());
                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "\"insert\":\""+flag+"\"";
    }

    public ResultSet copyQueryResult(String query){
        ResultSet rs = null;
        try {

            Statement stmt = connection.createStatement();
            //TestLog.stepInfo("Firing select query :" + query);
            rs = stmt.executeQuery(query);
            writefile.writeToFile("\n"+query+"\n");
            //TestLog.stepInfo("Select query has been successfully executed");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        return rs;
    }

    public String pasteQueryResult(String query, ResultSet rs) throws SQLException {
        String flag = null;
        String tableName = query.split("INSERT INTO ")[1].split(" \\(")[0];
        //if (tableName.equals("loan_finances")) {
            //String deleteQuery = "DELETE FROM " + tableName + " WHERE loan_application_id_ref = \'" + rs.getString("loan_application_id_ref") + "\'";
            //String deleteFromTableResult = deleteFromTable(deleteQuery);
        //}
        try {
            //TestLog.stepInfo("Firing update query :" + query);
            PreparedStatement ps = null;
            String newQuery = query;
            int columnCount = rs.getMetaData().getColumnCount();
            if(!query.contains("?")){
               newQuery=query + " (?";
               for(int i=2; i<=columnCount; i++){
                   newQuery+=",?";
               }
               newQuery+=")";
            }

            flag = "success";
            while (rs.next()) {
                try {
                    ps = connection.prepareStatement(newQuery);
                    writefile.writeToFile("\n"+newQuery+"\n");
                    String value = null;
                    for (int i = 1; i <= columnCount; i++) {
                        try {
                            if(rs.getObject(i)!=null) {
                                if(rs.getObject(i).toString().equalsIgnoreCase("true")){
                                    ps.setObject(i, 1);
                                }
                                else if(rs.getObject(i).toString().equalsIgnoreCase("false")){
                                    ps.setObject(i, 0);
                                }
                                else if((rs.getObject(i).toString().split(":").length==3) && (rs.getObject(i).toString().split("-").length==3) && !(rs.getObject(i).toString().length()>23)) {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(rs.getTimestamp(i));
                                    java.sql.Date sd = new java.sql.Date(cal.getTime().getTime());
                                    value = sdf.format(sd);
                                    ps.setObject(i, value);
                                }
                                else {
                                    value = rs.getObject(i).toString();
                                    ps.setObject(i, value);
                                }
                            }
                            else {
                                ps.setObject(i, null);
                            }
                        }
                        catch (Exception e) {
                            System.err.println("Got an exception! ");
                            System.err.println(e.getMessage());
                            flag = "failure";
                        }
                    }
                    ps.executeUpdate();
                    flag = "success";
                } catch (Exception e) {
                    if (e.getMessage().contains("Duplicate entry")) {
                        String primaryKeyName = rs.getMetaData().getColumnName(1);
                        String primaryKeyValue = rs.getObject(primaryKeyName).toString();
                        // To delete the duplicate row
                        String deleteQuery = "DELETE FROM " + tableName + " WHERE " + primaryKeyName + " = \'" + primaryKeyValue + "\'";
                        String deleteFromTableResult = deleteFromTable(deleteQuery);
                        if (deleteFromTableResult.contains("failure")) {
                            try {
                                if (rs.getString("loan_application_id_ref") != null) {
                                    deleteQuery = "DELETE FROM " + tableName + " WHERE loan_application_id_ref = \'" + rs.getString("loan_application_id_ref") + "\'";
                                    deleteFromTableResult = deleteFromTable(deleteQuery);
                                }
                            } catch (SQLException ex) {
                                try {
                                    if (rs.getString("loan_application_id") != null) {
                                        deleteQuery = "DELETE FROM " + tableName + " WHERE loan_application_id = \'" + rs.getString("loan_application_id") + "\'";
                                        deleteFromTableResult = deleteFromTable(deleteQuery);
                                    }
                                } catch (SQLException exc) {
                                    try {
                                        if (rs.getString("loan_app_id") != null) {
                                            deleteQuery = "DELETE FROM " + tableName + " WHERE loan_app_id = \'" + rs.getString("loan_app_id") + "\'";
                                            deleteFromTableResult = deleteFromTable(deleteQuery);
                                        }
                                    } catch (SQLException exce) {
                                        try {
                                            if (rs.getString("email") != null) {
                                                deleteQuery = "DELETE FROM " + tableName + " WHERE email = \'" + rs.getString("email") + "\'";
                                                deleteFromTableResult = deleteFromTable(deleteQuery);
                                            }
                                        } catch (SQLException e1) {
                                            try {
                                                if (rs.getString("pan_number") != null) {
                                                    deleteQuery = "DELETE FROM " + tableName + " WHERE pan_number = \'" + rs.getString("pan_number") + "\'";
                                                    deleteFromTableResult = deleteFromTable(deleteQuery);
                                                }
                                            } catch (SQLException e11) {
                                                try {
                                                    if (rs.getString("destination_loan_application_id_ref") != null) {
                                                        deleteQuery = "DELETE FROM " + tableName + " WHERE destination_loan_application_id_ref = \'" + rs.getString("destination_loan_application_id_ref") + "\'";
                                                        deleteFromTableResult = deleteFromTable(deleteQuery);
                                                    }
                                                } catch (SQLException e12) {
                                                    try {
                                                        if (rs.getString("ficcl_id") != null) {
                                                            deleteQuery = "DELETE FROM " + tableName + " WHERE ficcl_id = \'" + rs.getString("ficcl_id") + "\'";
                                                            deleteFromTableResult = deleteFromTable(deleteQuery);
                                                        }
                                                    } catch (SQLException e13) {
                                                        e13.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                    }
                        if(deleteFromTableResult.equalsIgnoreCase("\"delete\":\"success\"")) {
                            rs.previous();
                            //flag = "success";
                            continue;
                        }
                        else {
                            flag = "failure";
                        }
                    }
                    else {
                        System.err.println("Got an exception! ");
                        System.err.println(e.getMessage());
                        e.printStackTrace();
                        flag = "failure";
                    }
                }
                //TestLog.stepInfo("Update query has been done successfully executed");
            }
        } catch (Exception e) {
                //TestLog.stepInfo("Problem in update query, exception " + e.getMessage());
                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
                flag = "failure";
        }

        return "\"insert\":\""+flag+"\"";
    }

    public String deleteFromTable (String query) {
        String flag = "failure";
        try {
            try {
                //TestLog.stepInfo("Firing Delete query :" + query);
                Statement stmt = connection.createStatement();
                stmt.executeUpdate(query);
                //writefile fw = new writefile("databaseQueryResult.txt");
                writefile.writeToFile("\n"+query+"\n");
                flag = "success";
                //TestLog.stepInfo("Delete query has been done successfully executed");
            } catch (Exception e) {
                //TestLog.stepInfo("Problem in Delete query, exception " + e.getMessage());
                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "\"delete\":\""+flag+"\"";
    }

}
