import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class writefile {
    static String fileName;

    @SuppressWarnings("static-access")
    public writefile(String fileName) {
        super();
        this.fileName = fileName;
    }

    public static void writeToFile(String text){

            try {
                String FILENAME = fileName;
                if (FILENAME == null)
                    FILENAME = "databaseQueryResult.txt";

                readfile fr = new readfile(FILENAME);
                String data = fr.ReadFromFile();
                FileWriter fw = new FileWriter(FILENAME,true);
                BufferedWriter bw = new BufferedWriter(fw);
                if (data.endsWith("---END---,")) {
                    fw = new FileWriter(FILENAME,false);
                    bw = new BufferedWriter(fw);
                    bw.flush();
                    fw.flush();
                }
                bw.write(text);
                bw.flush();
                //fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        System.out.println("Write Success...");
    }
} 